# Schedule

| Date    | Week | Lecture | Topics                                | Assignments / Activities                                   | Reading                   |
| -----   | ---- | ------- | ------                                | ------------------------                                   | -------                   |
| Aug. 18 |  1   |  1      | Introduction to Jacobi & Gauss-Seidel | Compare dense and sparse direct solves with Jacobi and G-S | Saad 4.1                  |
| Aug. 20 |  1   |  2      | Convergence of Jacobi & Gauss-Seidel  | Demonstrate the divergence of some nonregular splittings   | Saad 1.8.4, 1.10, 4.2     |
| Aug. 25 |  2   |  3      |                                       |                                                            |                           |
| Aug. 27 |  2   |  4      |                                       |                                                            |                           |
| Sep. 1  |  3   |  5      |                                       |                                                            |                           |
| Sep. 3  |  3   |  6      |                                       |                                                            |                           |
| Sep. 8  |  4   |  7      |                                       |                                                            |                           |
| Sep. 10 |  4   |  8      |                                       |                                                            |                           |
| Sep. 15 |  5   |  9      |                                       |                                                            |                           |
| Sep. 17 |  5   | 10      |                                       |                                                            |                           |
| Sep. 22 |  6   | 11      |                                       |                                                            |                           |
| Sep. 24 |  6   | 12      |                                       |                                                            |                           |
| Sep. 29 |  7   | 13      |                                       |                                                            |                           |
| Oct. 1  |  7   | 14      |                                       |                                                            |                           |
| Oct. 6  |  8   | 15      |                                       |                                                            |                           |
| Oct. 8  |  8   | 16      |                                       |                                                            |                           |
| Oct. 13 |  9   | 17      |                                       |                                                            |                           |
| Oct. 15 |  9   | 18      |                                       |                                                            |                           |
| Oct. 20 | 10   | 19      |                                       |                                                            |                           |
| Oct. 22 | 10   | 20      |                                       |                                                            |                           |
| Oct. 27 | 11   | 21      |                                       |                                                            |                           |
| Oct. 29 | 11   | 22      |                                       |                                                            |                           |
| Nov. 3  | 12   | 23      |                                       |                                                            |                           |
| Nov. 5  | 12   | 24      |                                       |                                                            |                           |
| Nov. 10 | 13   | 25      |                                       |                                                            |                           |
| Nov. 12 | 13   | 26      |                                       |                                                            |                           |
| Nov. 17 | 14   | 27      |                                       |                                                            |                           |
| Nov. 19 | 14   | 28      |                                       |                                                            |                           |
| Nov. 24 | 15   | 29      |                                       |                                                            |                           |

## Topics

### Review

- Matrices (Saad 1.1)
- Square matrices and eigenvalues (Saad 1.2)
- Types of matrices (Saad 1.3)
- Vector inner products and norms (Saad 1.4)
- Matrix norms (Saad 1.5)
- Subspaces, range and kernel (Saad 1.6)
- Orthogonal vectors and subspaces (Saad 1.7)
- Canonical forms of matrices (diagonal, Jordan, Schur) (Saad 1.8)
- Normal and Hermitian matrices (Saad 1.9)
- Nonnegative matrices, M-matricecs (Saad 1.10)
- Positive-definite matrices (Saad 1.11)
- Projection operators (Saad 1.12)
- Basic concepts in linear systems (1.13)

