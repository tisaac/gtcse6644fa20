
all: syllabus.pdf syllabus.html

syllabus.pdf: README.tex.md
	pandoc -V geometry:margin=1in --metadata=linkcolor:blue -t latex -o syllabus.pdf --lua-filter ./utils/linebreak.lua README.tex.md

syllabus.html: README.tex.md
	pandoc -o syllabus.html --mathjax -f markdown+tex_math_dollars README.tex.md

clean:
	rm -rf syllabus.pdf syllabus.html

.PHONY: clean
