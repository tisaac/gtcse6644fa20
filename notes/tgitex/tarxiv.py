#!/usr/bin/env python

import os,sys,tempfile,subprocess,shutil

def flsGetInputs(flsname):
  abstopdir = os.path.abspath(os.curdir)
  fdb = open(flsname,'r')
  inFiles = []
  generatedFiles = []
  for line in fdb:
    tokens = line.split()
    if tokens[0] == 'INPUT' or tokens[0] == 'OUTPUT':
      thisfile = tokens[1]
      if os.path.isfile(thisfile):
        if not os.path.isabs(thisfile):
          thisfile = os.path.abspath(thisfile)
        if thisfile.startswith(abstopdir):
          thisfile = os.path.relpath(thisfile)
          if tokens[0] == 'INPUT':
            inFiles.append(thisfile)
          else:
            generatedFiles.append(thisfile)
  fdb.close()
  trueInFiles = set(inFiles) - set(generatedFiles)
  return list(trueInFiles)

def fdbGetInputs(fdbname):
  abstopdir = os.path.abspath(os.curdir)
  fdb = open(fdbname,'r')
  started = False
  generated = False
  inFiles = []
  generatedFiles = []
  for line in fdb:
    tokens = line.split()
    if tokens[0].startswith('['):
      if tokens[0] == '["pdflatex"]':
        started = True
        generated = False
      else:
        started = False
        generated = False
    elif tokens[0] == '(generated)':
      generated = True
    elif started:
      thisfile = tokens[0].strip('"')
      if os.path.isfile(thisfile):
        if not os.path.isabs(thisfile):
          thisfile = os.path.abspath(thisfile)
        if thisfile.startswith(abstopdir):
          thisfile = os.path.relpath(thisfile)
          (thisroot,thisext) = os.path.splitext(thisfile)
          if thisext == '.fmt':
            thisfls = thisroot + '.fls'
            theseinputs = flsGetInputs(thisfls)
            inFiles = inFiles + theseinputs
          else:
            inFiles.append(thisfile)
            if generated:
              generatedFiles.append(thisfile)
  fdb.close()
  trueInFiles = set(inFiles) - set(generatedFiles)
  return list(trueInFiles)

def TarxivFromLatexmk(fdbname,texinputspath):
  (targetroot,textext) = os.path.splitext(fdbname)
  targettopdir = os.path.basename(os.path.normpath(os.path.abspath(os.path.dirname(fdbname))))
  symfiles = []
  texdirs = [ os.path.normpath(x) for x in filter(None,texinputspath.split(':')) ]
  inputFiles = fdbGetInputs(fdbname)
  for idx, filename in enumerate(inputFiles):
    for dirname in texdirs:
      if filename.startswith(dirname):
        basename = os.path.basename(filename)
        if os.path.isfile(basename):
          raise
        else:
          shutil.copy(filename,basename)
          symfiles.append(basename)
        inputFiles[idx] = basename
  subprocess.call(["tar","-czvf",targetroot+".tgz","-C",".."]+[os.path.join(targettopdir,x) for x in inputFiles])
  for filename in symfiles: os.remove(filename)
  return

if __name__ == '__main__':
  TarxivFromLatexmk(sys.argv[1],sys.argv[2])
