\LoadClass{letter}
\usepackage{graphicx}
\usepackage{xcolor}
\definecolor{gttechgold}{RGB}{179,163,105}
\definecolor{gtlightgray}{RGB}{229,229,229}
\definecolor{gtdarkgray}{RGB}{84,84,84}
%\usepackage[lining]{montserrat}
\usepackage{libertine}
\usepackage[T1]{fontenc}
\usepackage[text={5.833in,10in}]{geometry}
\usepackage{microtype}

\pagestyle{empty}

\newcommand{\theheading}{%
  %\parskip=4pt
  \parindent=0pt%
  \hspace{-49pt}%
  %\begin{minipage}{6in}
    %\hfill
    \includegraphics{School-of-CSE-Outline-Logo}%
    %\end{minipage}%\hfill%\hspace{3.8in}%
    %\textcolor{gtlightgray}{\vrule width 0.65pt}\,\,\,%
    %\begin{minipage}{1.97in}%
    %  \textcolor{gttechgold}{\textbf{School of Computational Science and Engineering}}%
    %\end{minipage}
  %\end{minipage}%

  \smallskip

  %\hspace{-0.75in}%
  %\begin{minipage}{7.5in}%
  %  \large%
  %  \hfill%\hspace{5.5in}%
  %  \begin{minipage}{2.84in}%
      \normalsize%
      {\textbf{Tobin Isaac}}\\
      \textcolor{gtdarkgray}{%
        \fontseries{l}\selectfont
        Assistant Professor\\
        266 Ferst Drive\\
        Atlanta, GA 30332-0765
        \\
        tisaac@cc.gatech.edu\\
        404.385.5970\hfill Testing%
      }%
  %  \end{minipage}%
  %\end{minipage}%

  %\strut

  %\sffamily
  %\today
}

%
%	macro for address
%
\newcommand{\theaddress}[1]{%
  {\obeylines 
      #1
  }

  \strut
}
%
%	macro for opening
%
\newcommand{\theopening}[1]{
  {\obeylines \parindent=0in \parskip=0pt
     #1
  } 

}
%
%	macro for closing
%
\newcommand{\theclosing}[2]{
   \vskip.2in
   {\parskip=0pt%
     #1

     \strut

     \strut

     \strut

     \strut

     \parbox{3in}{#2} 
   } 
}
%
\newcommand{\thecc}[1]{

  \strut

  CC: #1
}
