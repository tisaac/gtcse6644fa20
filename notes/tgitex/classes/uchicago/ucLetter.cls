\LoadClass{letter}
\usepackage{graphicx}
\usepackage{xcolor}
\definecolor{ucmaroon}{RGB}{128,0,0}
\definecolor{uclightgray}{RGB}{214,214,206}
\definecolor{ucdarkgray}{RGB}{118,118,118}
\usepackage[lining]{montserrat}
\usepackage[text={6in,10in},centering]{geometry}

\pagestyle{empty}

\newcommand{\theheading}{%
  %\parskip=4pt
  \parindent=0pt
  \hspace{-0.75in}%
  \begin{minipage}{7.5in}
    \large%
    \begin{minipage}{2.5in}%
      \includegraphics[height=0.49in]{logo}%
    \end{minipage}\hfill%\hspace{3.8in}%
    \textcolor{uclightgray!85!black}{\vrule width 0.65pt}\,\,\,%
    \begin{minipage}{1.97in}%
      \textcolor{ucmaroon}{\textbf{Computation\\ Institute}}%
    \end{minipage}
  \end{minipage}%

  \smallskip

  \hspace{-0.75in}%
  \begin{minipage}{7.5in}%
    \large%
    \hfill%\hspace{5.5in}%
    \begin{minipage}{1.97in}%
      \normalsize%
      \textcolor{ucdarkgray}{\textbf{Tobin Isaac}}\\
      \textcolor{uclightgray!65!black}{%
        \fontseries{l}\selectfont
        Postdoctoral Scholar\\
        201 East 24th St., Stop C0200\\
        Austin, Texas 78712-1229\\
        \\
        tisaac@uchicago.edu\\
        512.232.7363%
      }%
    \end{minipage}%
  \end{minipage}%

  \strut

  \sffamily
  \today
}

%
%	macro for address
%
\newcommand{\theaddress}[1]{%
  {\obeylines 
      #1
  }

  \strut
}
%
%	macro for opening
%
\newcommand{\theopening}[1]{
  {\obeylines \parindent=0in \parskip=0pt
     #1
  } 

}
%
%	macro for closing
%
\newcommand{\theclosing}[2]{
   \vskip.2in
   {\parskip=0pt%
     #1

     \strut

     \strut

     \strut

     \strut

     \parbox{3in}{#2} 
   } 
}
%
\newcommand{\thecc}[1]{

  \strut

  CC: #1
}
