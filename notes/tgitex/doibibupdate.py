#!/usr/bin/env python3
'''
Take a list of doi and arxiv identifiers and update an autogenerated
bibtex/biber file with any that are not present
'''

import bibtexparser

def permissiveParser():
    parser = bibtexparser.bparser.BibTexParser()
    parser.ignore_nonstandard_types = False
    return parser

def getDois(stream):
    from yaml import load,dump
    try:
        from yaml import CLoader as Loader, CDumper as Dumper
    except ImportError:
        from yaml import Loader, Dumper

    return load(stream,Loader=Loader)

def getBibs(stream):
    bibstr = stream.read()
    return bibtexparser.loads(bibstr,parser=permissiveParser()).entries

def arxivGetBib(arxiv):
    import pycurl
    import xmltodict
    import re
    from io import BytesIO,StringIO

    buf = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.FOLLOWLOCATION, True)
    c.setopt(c.URL, 'http://export.arxiv.org/api/query?id_list=' + arxiv)
    c.setopt(c.WRITEDATA, buf)
    c.perform()
    c.close()

    body = buf.getvalue().decode('utf-8')
    out = xmltodict.parse(body)[u'feed'][u'entry']
    m = re.search(u'[0-9]+$',out[u'id'])
    v = m.group(0)
    db = bibtexparser.bibdatabase.BibDatabase()
    try:
        authors = u' and '.join([a[u'name'] for a in out[u'author']])
    except TypeError:
        authors = out[u'author'][u'name']
    db.entries = [{
        u'ENTRYTYPE'  : u'Online',
        u'author'     : authors,
        u'title'      : out[u'title'],
        u'date'       : out[u'updated'][0:10],
        u'version'    : v,
        u'eprinttype' : u'arXiv',
        u'eprintclass': out[u'arxiv:primary_category'][u'@term'],
        u'eprint'     : out[u'id'].replace(u'http://arxiv.org/abs/',u'')}]
    return db


def doiGetBib(doi):
    import pycurl
    from io import BytesIO,StringIO

    buf = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.FOLLOWLOCATION, True)
    c.setopt(c.HTTPHEADER, ["Accept: text/bibliography; style=bibtex"])
    c.setopt(c.URL, 'http://dx.doi.org/' + doi)
    c.setopt(c.WRITEDATA, buf)
    c.perform()
    c.close()

    body=buf.getvalue().decode('utf-8')
    #body=body.replace(',',',\n',1).replace('},','},\n')
    out=StringIO()
    count = 0
    for char in body:
        uchar = char
        if char == u'{':
            count += 1
        elif char == u'}':
            count -= 1
            if count == 0:
                out.write(u'\n')
        out.write(uchar)
        if char == u',' and count == 1:
            out.write(u'\n')

    out=bibtexparser.loads(out.getvalue(),parser=permissiveParser())

    return out
   
if __name__ == '__main__':
    import os,sys,argparse,codecs

    parser = argparse.ArgumentParser(description='Sync doi list with bibtex/biber file')
    parser.add_argument('--doi-file','-d', help='yaml file consisting of a list of {doi/arxiv,bibkey} pairs',type=argparse.FileType('r'),default=os.devnull)
    parser.add_argument('--bib-file','-b', help='bibtex/biber file to compare against',type=str)
    args = parser.parse_args()

    dois = getDois(args.doi_file)

    if args.bib_file:
        if os.path.isfile(args.bib_file):
            with codecs.open(args.bib_file,'r','utf-8') as stream:
                bibs = getBibs(stream)
        outstream = codecs.open(args.bib_file,'a','utf-8')
    else:
        bibs = []
        outstream = sys.stdout

    for key in dois:
        found = False
        for bib in bibs:
            if str(bib['ID']) == str(key):
                found = True
                break
        if found: continue
        ref = dois[key]
        (tag,idx) = ref.split(':')
        if tag == 'doi':
            bib = doiGetBib(idx)
        elif tag == 'arxiv':
            bib = arxivGetBib(idx)
        bib.entries[0][u'ID'] = key
        entry = bibtexparser.dumps(bib)
        outstream.write(entry)

    if outstream is not sys.stdout:
        outstream.close()

