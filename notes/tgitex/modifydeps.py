#!/usr/bin/env python

import os,sys,tempfile

def LocalRelativePath(filedir,thispath):
  if filedir:
    return os.path.normpath(os.path.relpath(thispath,filedir))
  else:
    return os.path.normpath(thispath)

def MakeDepsRelative(filename,topdir):
  '''wrap $(call thisdir,) around relative paths in a dependencies file'''
  filedir = os.path.dirname(filename)
  tmp = tempfile.TemporaryFile()
  try:
    inf = open(filename,'r')
    for line in inf:
      if line.startswith('#'):
        tmp.write(line)
        continue
      words = line.split()
      newWords=[]
      isRule = False
      for word in words:
        if os.path.isfile(word) and os.path.samefile(word,filename):
          continue
        if word.startswith('$(call'):
          tmp.close()
          return
        if word.startswith(':'):
          isRule = True
          newWords.append(word)
        elif not os.path.isabs(word):
          if word.endswith('\\'):
            newWords.append('$(call thisdir,'+LocalRelativePath(filedir,word.rstrip('\\'))+')\\')
          elif word.endswith(':'):
            isRule = True
            newWords.append('$(call thisdir,'+LocalRelativePath(filedir,word.rstrip(':'))+'):')
          else:
            newWords.append('$(call thisdir,'+LocalRelativePath(filedir,word)+')')
        elif word.startswith(topdir):
          newWords.append(word)
      if newWords:
        tmp.write(('' if isRule else '  ')+' '.join(newWords)+'\n')
    inf.close()
    tmp.seek(0)
    outf = open(filename,'w')
    for line in tmp:
      outf.write(line)
    outf.close()
  finally:
    tmp.close()
  return

if __name__ == '__main__':
  MakeDepsRelative(sys.argv[1],sys.argv[2])
