import numpy as np
import matplotlib as mpl
import warnings
import matplotlib.colors as colors
from scipy.sparse import spmatrix, csr_matrix, diags, eye
from scipy.sparse.linalg import eigs

warnings.filterwarnings("ignore", message="Changing the sparsity")
warnings.filterwarnings("ignore", message="default base")
warnings.filterwarnings("ignore", message="spsolve requires")

# colors for use in plotting
gtbuzzgold = (234/255,170/255,0)
gthorizon = (249/255,94/255,16/255)
gtlinkhoverblue = (24/255,121/255,219/255)
gtlightgray = (229/255,) * 3
gtdarkgray = (84/255,) * 3
gtblack85 = (38/255,) * 3

# default matplotlib colorscheme for slides
mpl.rcParams['axes.titlecolor'] = 'white'
mpl.rcParams['axes.edgecolor'] = 'white'
mpl.rcParams['axes.facecolor'] = 'white'
mpl.rcParams['figure.facecolor'] = gtblack85
mpl.rcParams['xtick.color'] = 'white'
mpl.rcParams['ytick.color'] = 'white'
mpl.rcParams['axes.labelcolor'] = 'white'
mpl.rcParams['figure.autolayout'] = True

def colorspy(ax, A, **kwargs):
    '''A better spy plot'''
    if not isinstance(A, np.ndarray):
        A = A.toarray()
    Amax = np.max(np.abs(A))
    vmin = kwargs.get('vmin', -Amax)
    vmax = kwargs.get('vmax', Amax)
    ax.set(xticks=[], yticks=[], aspect='equal')
    kw = kwargs.copy()
    kw['cmap'] = kwargs.get('cmap', 'PiYG')
    if len(A.shape) == 1:
        A = A[:,np.newaxis]
    if 'norm' in kw:
        return ax.pcolormesh(A[::-1,:], **kw)
    else:
        return ax.pcolormesh(A[::-1,:], vmin=vmin, vmax=vmax, **kw)


def sym_log_norm(A):
    '''symmetric log normalization for colorbars, scaled to an array or number'''
    if isinstance(A, spmatrix):
        Amax = np.max(np.abs(A.data))
    elif isinstance(A, np.ndarray):
        Amax = np.max(np.abs(A))
    else:
        Amax = abs(A)
    return colors.SymLogNorm(linthresh=Amax*1.e-16, vmin=-Amax, vmax=Amax)


def sym_log_norm_ticks(A, Amax=None):
    '''appropriate ticks for sym_log_norm(A)'''
    if Amax is None:
        if isinstance(A, spmatrix):
            Amax = np.max(np.abs(A.data))
        else:
            Amax = np.max(np.abs(A))
    maxpow = int(np.floor(np.log10(Amax)))
    step = (16 + maxpow) // 2
    return [-10**maxpow, -10**(maxpow-step), 0, 10**(maxpow-step), 10**maxpow]

def random_sparse_matrix(n=10, phi=0.3, L_matrix=False, positive_diagonals=False, diagonally_dominant=False, shift=0.):
    nnz = int(n * n * phi)
    # create some random coordinate entries
    row = np.random.randint(0, n, (nnz,))
    col = np.random.randint(0, n, (nnz,))
    data = np.random.rand(nnz) * 2. - 1
    A = csr_matrix((data, (row, col)), shape=(n, n))
    if L_matrix:
        positive_diagonals = True
    if positive_diagonals:
        A[range(n),range(n)] = np.random.rand(n)
    if L_matrix:
        A = np.absolute(A)
        A = 2. * diags(A.diagonal()) - A
    if diagonally_dominant:
        A[range(n),range(n)] = np.abs(A.diagonal())
        Aabs = A.copy()
        Aabs.data = np.abs(Aabs.data)
        Aabs[range(n),range(n)] *= -1.
        rowsum = Aabs @ np.ones(n)
        A[range(n),range(n)] += rowsum
    A[range(n),range(n)] += shift
    return A

def rho(G):
    l, v = eigs(G, k=1, which='LM')
    return abs(l[0])

def G_J(A):
    n = A.shape[0]
    Dinv = diags(1. / A.diagonal())
    return eye(n) - Dinv @ A
