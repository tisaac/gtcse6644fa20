--- Transform a raw HTML element which contains only a `<br>`
-- into a format-indepentent line break.
-- (https://stackoverflow.com/questions/28283008/preserve-line-breaks-in-title-using-pandoc)
function RawInline (el)
  if el.format:match '^html' and el.text:match '%<br ?/?%>' then
    return pandoc.LineBreak()
  end
end
