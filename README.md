# CSE 6644 / MATH 6644 (Fall 2020, 3 Credit Hours) | <br/> Iterative Methods for Systems of Equations

## Location and Modality

**Update 9/15:** instruction for this class is now "remote synchronous".  Lectures will not be held in person in the Instructional Center.  In-person meetings can still be scheduled if desired.

This section is listed as a "technology enhanced" residential course.
Classes will be held **Tuesdays and Thursdays at 2:00-3:15 p.m. ~~in [Instr. Center 215][InstrCntr]~~**.
The technology enhancements are:

- Classes will be [broadcast and recorded on BlueJeans][bjlectures].
- When possible, we will use [Microsoft whiteboards][mswboard] for live, collaborative note taking that is available to in-person and remote participants.

## Contact

|                              | **Instructor**                  | **Teaching Assistant**         |
| ---                          | :------------------             | :------------------            |
| *name*                       | Toby Isaac                      | Ziyi "Francis" Yin             |
| *email*                      | [tisaac@cc.gatech.edu][myemail] | [ziyi.yin@gatech.edu][zyemail] |
| *office*                     | [CODA S1323][codaloc]           |                                |
| *virtual office hours*       | [BlueJeans][bjhours]            | [BlueJeans][zybjhours]         |
| *face-to-face office hours*  | by appointment                  |                                |

Until the pandemic abates, I will be limiting my time on campus: trying to find me in my office unannounced probably won't work.

## Course Description

This course introduces iterative methods to solve linear systems of equations
(<img src="svgs/70681e99f542745bf6a0c56bd4600b39.svg" align=middle width=50.6962137pt height=22.8310566pt/>), nonlinear systems of equations (<img src="svgs/bc9fe0ff608195f80226773c27ffb416.svg" align=middle width=64.0067703pt height=24.657534pt/>), and optimization
problems (<img src="svgs/54d022415ff5086c0ba7aea3df141f82.svg" align=middle width=70.41110505pt height=24.657534pt/>).

Consider just <img src="svgs/70681e99f542745bf6a0c56bd4600b39.svg" align=middle width=50.6962137pt height=22.8310566pt/>. From [previous courses](#prerequisites), you know that
you can use [Gaussian elimination][gaussian-elimination] to solve for <img src="svgs/332cc365a4987aacce0ead01b8bdcc0b.svg" align=middle width=9.3949878pt height=14.1552444pt/>.
Gaussian elimination is a _direct solution method_: an algorithm that computes
the exact solution (in exact arithmetic) <img src="svgs/332cc365a4987aacce0ead01b8bdcc0b.svg" align=middle width=9.3949878pt height=14.1552444pt/> after a fixed number of steps. But
this can require a lot of work: ~<img src="svgs/69b104384fb98e2ca8d49437b77c09a8.svg" align=middle width=16.4194239pt height=26.7617526pt/> operations if <img src="svgs/53d147e7f3fe6e47ee05b88b166bd3f6.svg" align=middle width=12.32879835pt height=22.4657235pt/> is a dense <img src="svgs/55a049b8f161ae7cfeb0197d75aff967.svg" align=middle width=9.86687625pt height=14.1552444pt/>-by-<img src="svgs/55a049b8f161ae7cfeb0197d75aff967.svg" align=middle width=9.86687625pt height=14.1552444pt/>
matrix.  So Gaussian elimination is not practical for many big matrices.
Furthermore, nonlinear systems like <img src="svgs/bc9fe0ff608195f80226773c27ffb416.svg" align=middle width=64.0067703pt height=24.657534pt/> and optimization problems
<img src="svgs/54d022415ff5086c0ba7aea3df141f82.svg" align=middle width=70.41110505pt height=24.657534pt/> do not generally have direct solution methods at all.

An _iterative method_ takes an approximate solution <img src="svgs/41a0912d0f46af38c7fa2115d8f0386e.svg" align=middle width=16.6610169pt height=14.1552444pt/> to one of these
problems and computes a better approximate solution <img src="svgs/eaf657bf932d6276b08b6d1ac23782bc.svg" align=middle width=33.30493815pt height=14.1552444pt/>.  How?  How much
better?  How quickly?  And for which problems?  These are the topics of this
course.

Iterative methods were developed, and continue to be developed, to solve real
problems.  Our goal in this course will be to demonstrate every theoretical
result through hands-on interactions with real problems using the [numpy] and
[scipy] packages in python.

## Prerequisites

[Numerical Linear Algebra (CSE/MATH 6643)][math6643] or equivalent. (Note that
Numerical Linear Algebra is a completely different course than [Linear
Algebra][math1554]. The latter is an undergraduate math course, sometimes
taught along with differential equations, while the former is a graduate level
course.) The assignments and activities will require programming in python with
[numpy] and [scipy].

## Learning Outcomes

Students will develop facility with iterative methods for the numerical solution of linear and nonlinear systems, and their analysis. The students will be able to:

- Given a linear or nonlinear system, choose an appropriate numerical solution method based on the properties of the system
- Evaluate a method for its convergence and computational cost, including parallel computing aspects
- Diagnose convergence problems of iterative solution methods
- Select or design a method or approach for preconditioning the solution of specific problems
- Use [scipy], [PETSc][petsc] or other numerical software for solving systems of equations

## Scope

The course content is roughly divided into four modules (later topics in each module may only be covered depending on time and interest.

1. Introduction and basic iterative methods
    - Sparse matrices
    - Jacobi, Gauss-Seidel and SOR iterations
    - Block variants
    - Parallel partitions and reorderings
    - Asynchronous iterations
2. Krylov subspace methods
    - Chebyshev iteration
    - Conjugate gradient, conjugate residual, and GMRES
    - Pipelined variants
    - Biorthogonal variants
    - Methods for the normal equations
3. Preconditioning
    - Incomplete factorizations
    - Multigrid
    - Domain decomposition
    - Saddle-point problems
4. Nonlinear equations and optimization
    - Newton's method
    - Line search and trust region methods
    - Secant methods (Broyden, BFGS)
    - Nonlinear least squares (Gauss-Newton, Levenberg-Marquardt)
    - Stochastic gradient descent

## Grading

- **10% Participation and preparation**: There will be readings and prerecorded lectures that will be necessary preparation for class.  Asking questions or helping your peers on [piazza][piazza], in class, or in [virtual office hours][bjhours] will demonstrate that you are engaging with the material.
- **30% In-class activities**: Classes will frequently start with small comprehension quizzes about the preparation materials, and we will work through related activities in class.  The quizzes will be administered through canvas, and the activities will involve programming in python, so please bring a computer to class.  Within the 48 hours after class starts, you can retake a quiz and/or resubmit in-class activities for full credit.  Activities may be submitted late for 80% credit until a week after the they were assigned, but not after that.  These late submission options are also available to those who are unable to attend a class in person.
- **50% Homework**: Each module will have one associated homework assignment, which will have a mix of textual (manually graded) and programming (autograded) questions.  Each homework will be released at the start of its module and due shortly after the module ends.  Homeworks submitted on time will get feedback within a week.  Each homework may be submitted late for 80% credit until the due date of the next homework.  This includes the chance to resubmit each homework once after receiving feedback.  The due dates, subject to change, will be:

    - *Homework 1:* September 8, before class; late submission October 6, before class
    - *Homework 2:* October 6, before class; late submission November 3, before class
    - *Homework 3:* November 3, before class; late submission December 1
    - *Homework 4:* December 1; late submission December 8

To allow time for the final project, the last homework will be shorter, covering only the first three lectures of the last module

- **10% Project**:  Teams of 3-4 students will collaborate on a group project with a topic of your choosing.  Teams and topics will have to be finalized by **November 10**.  Teams will submit recorded presentations by the end of the day before our final exam period.  During the final exam period, each team member will participate (by video conference) individually in a 5-10 minute question and answer interview with the instructor.  **Presentations will be due November 30** and **interviews will be scheduled on December 1**.  If there is a potential conflict with interviewing remotely that day, please let me know as soon as you become aware.

Grades will follow [the standard Georgia Tech grading system][grading].

## Audit and Pass/Fail

Please inform the instructor and TA if you are taking the course for audit or pass/fail. If you wish to take the course for audit credit, you will not do a final project, but you must attempt the remaining assignments (quizzes, activities, and homeworks) and score at least 20% on each of the homeworks. If you wish to take the course as pass/fail, you will have to do the final project, and you need to score 50% or higher to pass the course.

## Collaboration and Group Work

Quizzes should be taken individually, without interacting with peers.  Individual quizzes will indicate whether they are open-note or closed-note.

Collaboration with peers on activities and homeworks is encouraged, but it must be consensual and attributed.  Assignments will have space for a "collaboration statement", where you can say who you worked with, on which questions, and to what extent.  For example, "I worked with George Burdell" isn't enough; "George Burdell and I worked together on Q4, and Jane Doe helped me get the answer to Q5" is.

## Technology and Course Materials

- We will use our [canvas page][canvas] for announcements, quizzes, assignments, and distributing additional course materials such as recorded lectures a reading assignments.  It is also where you can track your grade over the course of the semester.  Our canvas is connected to BlueJeans, so you will be able to find links to meetings and recorded broadcasts there, too.

- I have set up a [piazza page][piazza] because students seem to prefer it for discussion.  I will check piazza for questions about the assigned reading and preparation before each class.

- Video conferencing tools provide collaborative whiteboards, but unfortunately they are not available on all clients.  That is why I will try to have a [Microsoft whiteboard][mswboard] open during class and office hours.  Our whiteboard is at <https://bit.ly/3k3MklZ>.

- All required reading will either have a copy on canvas or will be accessible online.  The first three modules follow very closely the structure of _Iterative Methods for Sparse Linear Systems_ (Y. Saad, 2003, SIAM), which is a highly recommended text.  You can [buy it from SIAM][saad-siam] (There is a 30\% discount for SIAM members, and Georgia Tech students can [join SIAM][siam-join] for free.); you can [read it via the GT Library][saad-library]; you can read a [free PDF from the author][saad-pdf].

- Class activities and homeworks will written in Python using Jupyter notebooks, relying mostly on [scipy].  Using, e.g., [Anaconda][anaconda], these notebooks can be run on Linux, OS X, or Windows.  Some materials may rely on the [PETSc library][petsc].  Our canvas page has a conda environment that will install PETSc on Linux or OS X, but not Windows.  I can help students manually install PETSc on Windows, but students will also be able to run assignments on the [PACE instructional cluster environment][pace] (details to follow).

## Additional Resources

- **Dennis, J.E., Jr. and Schnabel, R. B.**, _Numerical Methods for Unconstrained Optimization and Nonlinear Equations_, 1996 (SIAM).  
  [Buy it from SIAM][ds-siam]
- **Trefethen, L.N. and Bau, D., III**, _Numerical Linear Algebra_, 1997 (SIAM).  
  [Buy it from SIAM][tb-siam]
- **Golub, G. and Van Loan, C.F.**, _Matrix Computations_, 2013 (Johns Hopkins Univ. Press).  
  [Buy it from JHU press][gvl-jhu] (I suggest checking other sellers for used copies of any edition.)

## Course Schedule

The following schedule is subject to revision.

| Date    | \#  | Topic                                                              | Deadlines                                      |
| :------ | :-  | :---------------------------------------------                     | :---------------------------                   |
| Aug. 18 | 1.1 | Course introduction & sparse matrices                              |                                                |
| Aug. 20 | 1.2 | Introduction to Jacobi, Gauss-Seidel, & SOR iterations             |                                                |
| Aug. 21 |     |                                                                    | Drop without withdrawal                        |
| Aug. 25 | 1.3 | Convergence of Jacobi                                              |                                                |
| Aug. 27 | 1.4 | Convergence of Gauss-Seidel & SOR                                  |                                                |
| Sep. 1  | 1.5 | Parallelism: block Jacobi & reordered Gauss-Seidel                 |                                                |
| Sep. 3  | 1.6 | Parallelism: asynchronous Jacobi                                   |                                                |
| Sep. 8  | 2.1 | Matrix polynomials & Chebyshev iteration                           | Homework 1                                     |
| Sep. 10 | 2.2 | Krylov subspaces and projection methods                            |                                                |
| Sep. 15 | 2.3 | Arnoldi's & Lanczos's methods                                      |                                                |
| Sep. 17 | 2.4 | Conjugate gradient & conjugate residual methods                    |                                                |
| Sep. 22 | 2.5 | Convergence of conjugate gradient                                  |                                                |
| Sep. 24 | 2.6 | GMRES & GMRES(k)                                                   |                                                |
| Sep. 29 | 2.7 | Parallelism: block & pipelined Krylov methods                      |                                                |
| Oct. 1  | 2.8 | Biorthogonal Krylov methods                                        |                                                |
| Oct. 6  | 2.9 | Krylov methods for normal equations                                |                                                |
| Oct. 8  | 3.1 | Preconditioning: left, right, & two-sided; flexible Krylov methods | 80% Homework 1; Homework 2                     |
| Oct. 13 | 3.2 | Incomplete factorizations                                          |                                                |
| Oct. 15 | 3.3 | Introduction to geometric multigrid                                |                                                |
| Oct. 20 | 3.4 | Multigrid cycles & convergence                                     |                                                |
| Oct. 22 | 3.5 | Algebraic multigrid                                                |                                                |
| Oct. 24 |     |                                                                    | Withdrawal; grade mode change                  |
| Oct. 27 | 3.6 | Domain decomposition methods                                       |                                                |
| Oct. 29 | 3.7 | Saddle-point & Schur-complement preconditioners                    |                                                |
| Nov. 3  | 4.1 | Newton's method and its convergence                                | 80% Homework 2; Homework 3                     |
| Nov. 5  | 4.2 | Globalization: line search & trust region methods                  |                                                |
| Nov. 10 | 4.3 | Secant methods (Broyden, BFGS)                                     | Project teams and topics                       |
| Nov. 12 | 4.4 | Inexact Newton: Eisenstat-Walker forcing                           |                                                |
| Nov. 17 | 4.5 | Nonlinear least squares (Gauss-Newton, Levenberg-Marquardt)        |                                                |
| Nov. 19 | 4.6 | Nonlinear preconditioning                                          |                                                |
| Nov. 24 | 4.7 | Stochastic gradient descent                                        |                                                |
| Nov. 30 |     |                                                                    | Project presentations                          |
| Dec. 1  |     |                                                                    | 80% Homework 3; Homework 4; Project interviews |
| Dec. 8  |     |                                                                    | 80% Homework 4                                 |

## Health-related Considerations

Effective July 15, 2020, University System of Georgia (USG) institutions will require all faculty, staff, students, and visitors to wear an appropriate face covering while inside campus facilities/buildings where six feet social distancing may not always be possible. All members of the campus community will be provided reusable cloth face coverings. 

Face covering use will be in addition to and is not a substitute for social distancing. Anyone not using a face covering when required will be asked to wear one or must leave the area. Refusal to comply with the requirement may result in discipline through the applicable conduct code for faculty, staff or students. 

There are a few exemptions. Reasonable accommodations may also be made for those who are unable to wear a face covering for documented health reasons. For more information about face masks and coverings, review the [guidelines from Human Resources][guidehr].

## Seating

**Update 9/15:** There is no longer any in-person seating.

~~The institute would like us to use assigned seating, to make it easier to contact trace and reduces the opportunities for spreading disease.  If you intend to attend class in person, please mark down a spot on this [seat chart][seatchart] for yourself.~~

## Academic Integrity

Georgia Tech aims to cultivate a community based on trust, academic integrity, and honor. Students are expected to act according to the highest ethical standards.  For information on Georgia Tech's Academic Honor Code, please visit <http://www.catalog.gatech.edu/policies/honor-code/> or <http://www.catalog.gatech.edu/rules/18/>.

Any student suspected of cheating or plagiarizing on a quiz, exam, or assignment will be reported to the Office of Student Integrity, who will investigate the incident and identify the appropriate penalty for violations.

## Accommodations for Students with Disabilities

If you are a student with learning needs that require special accommodation, contact the Office of Disability Services at (404)894-2563 or <http://disabilityservices.gatech.edu/>, as soon as possible, to make an appointment to discuss your special needs and to obtain an accommodations letter.  Please also e-mail me as soon as possible in order to set up a time to discuss your learning needs.

## Student-Faculty Expectations Agreement

At Georgia Tech we believe that it is important to strive for an atmosphere of mutual respect, acknowledgement, and responsibility between faculty members and the student body. See [this catalog page](http://www.catalog.gatech.edu/rules/22/) for an articulation of some basic expectation that you can have of me and that I have of you. In the end, simple respect for knowledge, hard work, and cordial interactions will help build the environment we seek. Therefore, I encourage you to remain committed to the ideals of Georgia Tech while in this class.

## Resources for Students

In your time at Georgia Tech, you may find yourself in need of support. Below you will find some resources to support you both as a student and as a person.

### Academic support

- Center for Academic Success <http://success.gatech.edu>
    - 1-to-1 tutoring <http://success.gatech.edu/1-1-tutoring>
    - Peer-Led Undergraduate Study (PLUS) <http://success.gatech.edu/tutoring/plus>
- OMED: Educational Services (<http://omed.gatech.edu/programs/academic-support)>
    - Group study sessions and tutoring programs
- Communication Center (<http://www.communicationcenter.gatech.edu)>
    - Individualized help with writing and multimedia projects
- Advising and Transition (<https://advising.gatech.edu)>
    - Study Strategies Seminar course <https://advising.gatech.edu/gt2801-study-strategies-seminar>
    - Academic coaching <https://advising.gatech.edu/academic-coaching>
    - Advising in your major <http://advising.gatech.edu/>

### Personal Support

#### Georgia Tech Resources

- The Office of the Dean of Students:  <https://studentlife.gatech.edu/content/get-help-now>;          404-894-6367; Smithgall Student Services Building 2nd floor
    - You also may request assistance at <https://gatech-advocate.symplicity.com/care_report/index.php/pid383662?>
- Center for Assessment, Referral and Education (CARE) 404-894-3498; <https://care.gatech.edu/>
    - Smithgall Student Services Building 1st floor
    - Students seeking assistance from the Counseling Center or Stamps Psychiatry need to visit CARE first for a primary assessment and referral to on and off campus mental health and well-being resources. 
    - Students in crisis may walk in during business hours (8am-4pm, Monday through Friday) or contact the counselor on call after hours at 404-894-2575 or 404-894-3498. Other crisis resources: <https://counseling.gatech.edu/content/students-crisis>
- Students’ Temporary Assistance and Resources (STAR): <https://studentlife.gatech.edu/content/star-services>
    - Can assist with interview clothing, food, and housing needs.
- Stamps Health Services: <https://health.gatech.edu>; 404-894-1420
    - Primary care, pharmacy, women’s health, psychiatry, immunization and allergy, health promotion, and nutrition
- OMED: Educational Services:  <http://www.omed.gatech.edu>
- Women’s Resource Center:  <http://www.womenscenter.gatech.edu>; 404-385-0230
- LGBTQIA Resource Center:  <http://lgbtqia.gatech.edu/>; 404-385-2679
- Veteran’s Resource Center:  <http://veterans.gatech.edu/>; 404-385-2067
- Georgia Tech Police: 404-894-2500; <http://www.police.gatech.edu>

#### National Resources

- The National Suicide Prevention Lifeline | 1-800-273-8255
    - Free and confidential support 24/7 to those in suicidal or emotional distress
- The Trevor Project
    - Crisis intervention and suicide prevention support to members of the LGBTQ+ community and their friends
    - Telephone | 1-866-488-7386 | 24 hours a day, 7 days a week
    - Online chat | 24 hours a day, 7 days a week
    - Text message | Text “START” to 687687 | 24hrs day, 7 days a week

## Statement of Intent for Inclusivity

As a member of the Georgia Tech community, I am committed to creating a learning environment in which all of my students feel safe and included.  Because we are individuals with varying needs, I am reliant on your feedback to achieve this goal.  To that end, I invite you to enter into dialogue with me about the things I can stop, start, and continue doing  to make my classroom an environment in which every student feels valued and can engage actively in our learning community.

[myemail]: mailto:tisaac@cc.gatech.edu
[zyemail]: mailto:ziyi.yin@gatech.edu
[codaloc]: https://codatechsquare.com/location/
[canvas]: https://gatech.instructure.com/courses/153036
[piazza]: https://piazza.com/class/kdb34m3yrce6d6
[gaussian-elimination]: https://en.wikipedia.org/wiki/Gaussian_elimination
[saad-siam]: https://my.siam.org/Store/Product/viewproduct/?ProductId=982
[saad-library]: https://gatech-primo.hosted.exlibrisgroup.com/permalink/f/1vgrnp4/01GALI_GIT_ALMA51323800370002947
[saad-pdf]: https://www-users.cs.umn.edu/~saad/IterMethBook_2ndEd.pdf
[siam-join]: http://www.siam.org/students/memberships.php
[tb-siam]: https://my.siam.org/Store/Product/viewproduct/?ProductId=950
[ds-siam]: https://my.siam.org/Store/Product/viewproduct/?ProductId=755
[gvl-jhu]: https://jhupbooks.press.jhu.edu/title/matrix-computations
[numpy]: numpy.org
[scipy]: scipy.org
[math6643]: https://math.gatech.edu/courses/math/6643
[math1554]: https://math.gatech.edu/courses/math/1554
[InstrCntr]: https://map.gatech.edu/?id=82#!m/11007
[bjlectures]: https://canvaslms.bluejeansint.com/conferences/93538/start
[bjhours]: https://canvaslms.bluejeansint.com/conferences/93539/start
[zybjhours]: https://canvaslms.bluejeansint.com/conferences/94289/start
[mswboard]: https://whiteboard.microsoft.com/
[petsc]: https://www.mcs.anl.gov/petsc/
[grading]: https://registrar.gatech.edu/info/grading-system
[ourboardraw]: https://wbd.ms/share/v2/aHR0cHM6Ly93aGl0ZWJvYXJkLm1pY3Jvc29mdC5jb20vYXBpL3YxLjAvd2hpdGVib2FyZHMvcmVkZWVtL2M0YzI2MmEzMjk2MDRiMjk4OGFlYzg5NjRjMjgyNWI0XzQ4MjE5OGJiLWFlN2ItNGIyNS04YjdhLTZkN2YzMmZhYTA4Mw==
[ourboardbitly]: https://bit.ly/3k3MklZ
[anaconda]: https://anaconda.org
[pace]: https://pace.gatech.edu/pace-ice-instructional-cluster-environment-education
[guidehr]: https://hr.gatech.edu/face-coverings
[seatchart]: https://b.gatech.edu/2FEvZEF
