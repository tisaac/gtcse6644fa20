{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activity 2.3: Arnoldi & Lanczos iterations\n",
    "\n",
    "The goal of today's activity are:\n",
    "\n",
    "1. Demonstrate an understanding of **the difference between modified and classical Gram-Schmidt** for Arnoldi's method\n",
    "\n",
    "2. Explore how the **Rayleigh-Ritz eigenvalues** from Arnoldi's method compare to the true eigenvalues for generic and symmetric matrices\n",
    "\n",
    "8 points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Collaboration statement:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-3da772b13e623db5",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "%pip install numpy scipy matplotlib\n",
    "import numpy as np\n",
    "from scipy.linalg import eigvals, schur\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "from scipy.sparse import diags\n",
    "mpl.rcParams['figure.figsize'] = (13, 8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-99e586d9bc15b562",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "**Activity 1 (2 points):** Complete the implementation of Arnoldi's method using modified Gram-Schmidt orthogonalization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-91f675a638c2ae2e",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def arnoldi_modified_gram_schmidt(A, v, m, tol=1.e-10):\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        A (implements @): matrix\n",
    "        v ((n,) np.ndarray): initial vector for Krylov subspace\n",
    "    Returns:\n",
    "        (V ((n, m) np.array), H ((m+1, m) np.ndarray)): V is an orthogonal,\n",
    "            graded basis of the Krylov subspace K_m; H is the Hessenberg matrix.\n",
    "    \"\"\"\n",
    "    n = len(v)\n",
    "    V = np.zeros((n, m))\n",
    "    H = np.zeros((m+1, m))\n",
    "    v = v.copy()\n",
    "    v /= np.linalg.norm(v)\n",
    "    V[:,0] = v\n",
    "    for j in range(m):\n",
    "        w = A @ v\n",
    "        # store the dot product with each of the previous j+1 columns of V in H[j,0:j+1],\n",
    "        # and orthogonalize w against them in a modified Gram-Schmidt update\n",
    "        ### BEGIN SOLUTION\n",
    "        modified = True\n",
    "        if not modified:\n",
    "            H[:j+1,j] = w.dot(V[:,:j+1])\n",
    "            w -= V[:,:j+1] @ H[:j+1,j]\n",
    "        else:\n",
    "            for i in range(j+1):\n",
    "                H[i,j] = V[:,i].dot(w)\n",
    "                w -= V[:,i] * H[i,j]\n",
    "        ### END SOLUTION\n",
    "        H[j+1,j] = np.linalg.norm(w)\n",
    "        if H[j+1,j] < tol:\n",
    "            return V[:,:(j+1)], H[:(j+1),:(j+1)]\n",
    "        if j < m-1:\n",
    "            V[:,j+1] = v[:] = w / H[j+1, j]\n",
    "    return V, H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your implementation will be tested on a matrix and initial vector where the orthogonalization of $w = A v_k$ against $v_1, \\dots, v_k$ will be much smaller than the original vector, so that the errors in the dot products become significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-69d1afe564809c34",
     "locked": true,
     "points": 2,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "n = 5\n",
    "m = 4\n",
    "A = np.eye(n)\n",
    "A[[0, 1, 2, 3],[1, 2, 3, 4]] = 1.e1\n",
    "A[[0, 1, 2],[2, 3, 4]] = 1.e2\n",
    "A[[0, 1],[3, 4]] = 1.e3\n",
    "A[0, 4] = 1.e4\n",
    "print(A)\n",
    "v = np.ones(n)\n",
    "V, H = arnoldi_modified_gram_schmidt(A, v, m)\n",
    "m = V.shape[1]\n",
    "print(\"V orthogonality error\", np.linalg.norm(V.T @ V - np.eye(m)))\n",
    "assert(np.linalg.norm(V.T @ V - np.eye(m)) < 1.e-10)\n",
    "print(\"Krylov subspace error\", np.linalg.norm(V.T @ (A @ V) - H[:m, :]))\n",
    "assert(np.linalg.norm(V.T @ (A @ V) - H[:m, :]) < 1.e-7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Rayleigh-Ritz values from Arnoldi's method\n",
    "\n",
    "When we use a Krylov subspace to approximately solve a linear system, we are building approximations to $A^{-1} v$ out of polynomials of the form $p_m(A) v$.  For this purpose, we only need the projected solution to be a good approximation to the true solution.  But another related use of Arnoldi's method (and one that tells us something about the convergence of Krylov subspace methods) is to generate approximations to the spectrum $\\sigma(A)$ using the spectrum $\\sigma(H_m)$.  These are called the \"Ritz\" or \"Rayleigh-Ritz\" eigenvalues of the Krylov subspace.\n",
    "\n",
    "### General matrices\n",
    "\n",
    "Let's start by creating two random real matrices, $A$ and $B$.  $A$ will have normally distributed entries (plus a shift on the diagonal), and it will have both real eigenvalues and pairs of complex eigenvalues that are conjugate of each other.\n",
    "\n",
    "We will then create $B$ from $A$ by taking the Schur complement of $A$ (which will have the eigenvalues on the diagonal), and copying some of the eigenvalues on the diagonal repeatedly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-550663e8ad796653",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# Create a random matrix\n",
    "n = 100\n",
    "A = np.random.randn(n, n) + np.eye(n)*10\n",
    "# Compute its real Schur factorization\n",
    "T, Z = schur(A, output='real')\n",
    "# nonzeros below the diagonal to detect complex eigenvalue pairs\n",
    "complex_pair = np.where(T.diagonal(-1) != 0.)[0]\n",
    "# The complement are real eigenvalues\n",
    "real_eigs = sorted(list(set(range(n)) - set(complex_pair) - set(complex_pair+1)))\n",
    "# Duplicate the upper triangular factor that has eigenvalues on the diagonal\n",
    "Tdup = T.copy()\n",
    "# Say how many times we want each eigenvalue to appear in B\n",
    "m = int(n**(1./2.))\n",
    "# Copy eigenvalues\n",
    "off = 0\n",
    "while off < len(complex_pair):\n",
    "    offnext = min(len(complex_pair), off + m)\n",
    "    j = complex_pair[off]\n",
    "    for i in range(off+1,offnext):\n",
    "        k = complex_pair[i]\n",
    "        Tdup[k:k+2,k:k+2] = T[j:j+2,j:j+2]\n",
    "    off = offnext\n",
    "off = 0\n",
    "while off < len(real_eigs):\n",
    "    offnext = min(len(real_eigs), off + m)\n",
    "    j = real_eigs[off]\n",
    "    for i in range(off+1, offnext):\n",
    "        k = real_eigs[i]\n",
    "        Tdup[k,k] = T[j,j]\n",
    "    off = offnext\n",
    "# Form B\n",
    "B = Z @ Tdup @ Z.T\n",
    "# Calculate eigenvalues\n",
    "la = eigvals(A)\n",
    "lb = eigvals(B)\n",
    "# Plot\n",
    "ax = plt.figure().gca()\n",
    "ax.set_aspect('equal')\n",
    "_ = ax.scatter(np.real(la),np.imag(la), label=r'$\\sigma(A)$')\n",
    "_ = ax.scatter(np.real(lb), np.imag(lb), marker='+', label=r'$\\sigma(B)$')\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will use the same vector to create a Krylov subspace of size $n$ for each matrix.  This is the full dimension of the matrix, and because Krylov subspace only grows, it contains $H_k$ for each $k$ as the first $k \\times k$ block on its diagonal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-67543e752ec8e920",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# Create a random initial vector\n",
    "v = np.random.randn(n)\n",
    "VA, HA = arnoldi_modified_gram_schmidt(A, v, n)\n",
    "VB, HB = arnoldi_modified_gram_schmidt(B, v, n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 2 (3 points):** Plot the Rayleigh-Ritz values for $H_{A,k}$ and $H_{B,k}$ for some values of $k < n$, then answer the following questions:\n",
    "\n",
    "1. Which eigenvalues in $\\sigma(A)$ are well-approximated first?\n",
    "2. How does the convergence of $\\sigma(H_{A,k})\\to\\sigma(A)$ differ from $\\sigma(H_{B,k})\\to\\sigma(B)$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-cb984c8ae80ad4ad",
     "locked": false,
     "points": 1,
     "schema_version": 3,
     "solution": true,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "m = 50\n",
    "lha = eigvals(HA[:m,:m])\n",
    "plt.figure()\n",
    "plt.scatter(np.real(la),np.imag(la),marker='o', label=r'$\\sigma(A)$')\n",
    "plt.scatter(np.real(lha),np.imag(lha),marker='+', label=r'$\\sigma(H_{m}(A))$')\n",
    "plt.gca().set_aspect('equal')\n",
    "plt.gca().legend()\n",
    "\n",
    "lhb = eigvals(HB[:m,:m])\n",
    "plt.figure()\n",
    "plt.scatter(np.real(lb),np.imag(lb),marker='o', label=r'$\\sigma(B)$')\n",
    "plt.scatter(np.real(lhb),np.imag(lhb),marker='+', label=r'$\\sigma(H_{m}(B))$')\n",
    "plt.gca().set_aspect('equal')\n",
    "_ = plt.gca().legend()\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-040089afd74a4404",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "1. The \"outer\" eigenvalues of $\\sigma(A)$ are approximated better first.\n",
    "2. The Rayleigh-Ritz values converge slower to the repeated eigenvalues of $\\sigma(B)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Symmetric matrices\n",
    "\n",
    "We repeat the same type of experiment, but with a symmetric matrix $C$ and a matrix $D$ that has the same eigenvectors as $C$ but with duplicate eigenvalues.  Note that Arnoldi's method applied to a symmetric matrix is the same as Lanczos's method in exact arithmetic (though they will differ in finite precision because each new vector is only orthogonalized against the last two in Lanczos's method)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-d4fbbb44f106a3f5",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# Create a random set of positive eigenvalues\n",
    "S = np.random.randn(n)\n",
    "# Copy it\n",
    "Sdup = S.copy()\n",
    "# Set the duplication factor\n",
    "m = int(n**(1./2.))\n",
    "# Duplicate eigenvalues\n",
    "off = 0\n",
    "while off < n:\n",
    "    offnext = min(n, off + m)\n",
    "    Sdup[off:offnext] = S[off]\n",
    "    off = offnext\n",
    "# Create a random set of orthogonal vectors\n",
    "Q, _ = np.linalg.qr(np.random.randn(n,n))\n",
    "# C is an SPD matrix without duplicate eigenvalues\n",
    "C = Q @ diags(S) @ Q.T\n",
    "# D is an SPD matrix with duplicate eigenvalues\n",
    "D = Q @ diags(Sdup) @ Q.T\n",
    "# Calculate eigenvalues\n",
    "lc = eigvals(C)\n",
    "ld = eigvals(D)\n",
    "# Plot\n",
    "ax = plt.figure().gca()\n",
    "ax.set(ylim=(-1,1))\n",
    "_ = ax.scatter(np.real(lc),np.imag(lc), label=r'$\\sigma(A)$')\n",
    "_ = ax.scatter(np.real(ld), np.imag(ld), label=r'$\\sigma(B)$')\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-7aca72842f23fc70",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "v = np.random.randn(n)\n",
    "VC, HC = arnoldi_modified_gram_schmidt(C, v, n)\n",
    "VD, HD = arnoldi_modified_gram_schmidt(D, v, n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 3 (3 points):** Plot the Rayleigh-Ritz values for $H_{C,k}$ and $H_{D,k}$ for some values of $k < n$, then answer the following questions:\n",
    "\n",
    "1. Which eigenvalues in $\\sigma(C)$ and $\\sigma(D)$ are well-approximated first?\n",
    "2. How does the convergence of $\\sigma(H_{D,k})\\to\\sigma(D)$ differ from $\\sigma(H_{B,k})\\to\\sigma(B)$, which also had repeated eigenvalues?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-5f7fe51c7a286c11",
     "locked": false,
     "points": 1,
     "schema_version": 3,
     "solution": true,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "m = 10\n",
    "lhc = eigvals(HC[:m,:m])\n",
    "ax = plt.figure().gca()\n",
    "ax.set(ylim=(-1,1))\n",
    "ax.scatter(np.real(lc),np.imag(lc), label=r'$\\sigma(C)$')\n",
    "ax.scatter(np.real(lhc),np.imag(lhc), marker='x', label=r'$\\sigma(H_{m}(C))$')\n",
    "ax.legend()\n",
    "\n",
    "lhd = eigvals(HD[:m,:m])\n",
    "ax = plt.figure().gca()\n",
    "ax.set(ylim=(-1,1))\n",
    "ax.scatter(np.real(ld),np.imag(ld), label=r'$\\sigma(D)$')\n",
    "ax.scatter(np.real(lhd),np.imag(lhd), marker='x', label=r'$\\sigma(H_{m}(D))$')\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-ff735134d2f726b9",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "1. Same as before, the convex hull (maximum and minimum eigenvalues) are best approximated first.\n",
    "2. Unlike the generic case, the converge to repeated eigenvalues is fast: they are exactly captured for some $k < n$."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
