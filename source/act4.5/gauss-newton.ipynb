{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activity 4.5: Gauss-Newton\n",
    "\n",
    "3 points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**collaboration statement:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install scipy numpy matplotlib\n",
    "import numpy as np\n",
    "from scipy.optimize import minimize\n",
    "from scipy.sparse import eye, diags\n",
    "from scipy.sparse import rand as sprand\n",
    "from scipy.sparse.linalg import cg\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.sparse.linalg import LinearOperator\n",
    "plt.rcParams['figure.figsize'] = [8, 5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nonlinear regression\n",
    "\n",
    "Suppose we have a nonlinear model of a random process that looks like,\n",
    "\n",
    "$$d(x) := \\exp(C x)\\epsilon,$$\n",
    "\n",
    "where $C\\in\\mathbb{R}^{m\\times n}$, $m \\geq n$, is full rank, and $\\epsilon$ is a vector of independent, mean-1 random variables.\n",
    "\n",
    "For example, below $C$ is a collection of randomly chosen locations, $x$ is a scalar, and $\\epsilon$ is distributed like the gamma distribution Gamma(1,1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "def model_1d(m, n, x):\n",
    "    C = np.array(sorted(np.random.rand(m)))[:,np.newaxis]\n",
    "    epsilon = np.random.gamma(1.,1.,size=(m,))\n",
    "    z = C @ x\n",
    "    mu = np.exp(z)\n",
    "    d = mu * epsilon\n",
    "    return d, C\n",
    "\n",
    "m = 100\n",
    "n = 1\n",
    "Cgrid = np.linspace(0,1,m)[:,np.newaxis]\n",
    "x = np.array([4.])\n",
    "d, C = model_1d(m, n, x)\n",
    "mugrid = np.exp(Cgrid @ x)\n",
    "\n",
    "ax = plt.figure().gca()\n",
    "ax.set(xlabel='C', ylabel='d')\n",
    "ax.plot(Cgrid,mugrid, label=\"exp(Cx)\")\n",
    "ax.plot(C, d, 'x', label=\"d\")\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose we have $d$ and $C$ but we don't know $\\epsilon$ and we would like to estimate $x$.\n",
    "\n",
    "We could take the logarithm of both sides of the definition of $d$,\n",
    "\n",
    "$$ \\log d = C x + \\log \\epsilon,$$\n",
    "\n",
    "ignore the $\\log \\epsilon$ term, and try to find an estimate $x^*$ with ordinary least-squares,\n",
    "\n",
    "$$x^* = \\arg\\min_x \\tfrac{1}{2}\\|Cx - \\log d\\|_2^2,$$\n",
    "\n",
    "which has a linear optimality condition.\n",
    "\n",
    "But, the result is a biased estimate of $x$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plt.figure().gca()\n",
    "ax.plot(Cgrid,mugrid)\n",
    "ax.set(xlabel='C', ylabel='d')\n",
    "# run the linear estimator 100 times for randomly chosen locations C\n",
    "for i in range(100):\n",
    "    d, C = model_1d(m, n, x)\n",
    "    logd = np.log(d)\n",
    "    x_est = np.linalg.solve(C.T @ C, C.T @ logd)\n",
    "    mu_estgrid = np.exp(Cgrid @ x_est)\n",
    "    ax.plot(Cgrid, mu_estgrid, 'r:')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem is that the expectation $E[\\log(\\epsilon)] < \\log(E[\\epsilon]) = 0$ due to [Jensen's inequality](https://en.wikipedia.org/wiki/Jensen%27s_inequality).\n",
    "\n",
    "If we know enough about the distribution of $\\epsilon$ we could compute $E[\\log(\\epsilon)]$ and use that to unbias the estimate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plt.figure().gca()\n",
    "ax.plot(Cgrid,mugrid)\n",
    "ax.set(xlabel='C', ylabel='d')\n",
    "# run the linear estimator 100 times for randomly chosen locations C\n",
    "for i in range(100):\n",
    "    d, C = model_1d(m, n, x)\n",
    "    logd = np.log(d)\n",
    "    # unbiasing log(d) ............................ here\n",
    "    x_est = np.linalg.solve(C.T @ C, C.T @ (logd + np.euler_gamma))\n",
    "    mu_estgrid = np.exp(Cgrid @ x_est)\n",
    "    ax.plot(Cgrid, mu_estgrid, 'r:')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if the only thing we're confident that we know about the distribution of $\\epsilon$ is that its mean is 1, then we would have to estimate $E[\\log(\\epsilon)]$ along with $x$.\n",
    "\n",
    "We can avoid having to know more about $\\epsilon$ if we estimate $x^*$ using a _nonlinear least-squares problem_,\n",
    "\n",
    "$$x^* = \\arg\\min_x \\tfrac{1}{2}\\|\\exp(Cx) - d\\|_2^2 = \\tfrac{1}{2}\\|\\exp(Cx)E[\\epsilon] - d\\|^2 = \\tfrac{1}{2}\\|E[\\exp(Cx)\\epsilon - d]\\|^2.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plt.figure().gca()\n",
    "ax.set(xlabel='C', ylabel='d')\n",
    "ax.plot(Cgrid,mugrid)\n",
    "# run the linear estimator 100 times for randomly chosen locations C\n",
    "for i in range(100):\n",
    "    d, C = model_1d(m, n, x)\n",
    "    def f(x):\n",
    "        e = d - np.exp(C @ x)\n",
    "        return 0.5 * e.dot(e)\n",
    "    res = minimize(f, x)\n",
    "    x_est = res.x\n",
    "    mu_estgrid = np.exp(Cgrid @ x_est)\n",
    "    ax.plot(Cgrid, mu_estgrid, 'r:')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's set up a problem like this were $C$ is a sparse matrix and $x$ is a vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = 1000\n",
    "n = 500\n",
    "C = sprand(m, n, 0.1)\n",
    "print(np.linalg.cond((C.T @ C).toarray()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 1 (2 points):** Let $F(x) = \\exp(C x)$ and $f(x) = \\tfrac{1}{2} \\|F(x) - d\\|_2^2$.  If we approached the nonlinear least-squares problem as a minimization problem as we have already in the semester, we would either compute or approximate $\\nabla^2 f(x)$.  Today, however, we are exploring methods that only require $\\nabla F$.\n",
    "Compute $J = \\nabla F$ for this problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-2ac65d30292aea30",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def F(x, C):\n",
    "    z = C @ x\n",
    "    mu = np.exp(z)\n",
    "    return mu\n",
    "\n",
    "def J(x, C):\n",
    "    \"\"\"Return a sparse matrix representation of the Jacobian of F\"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    mu = F(x, C)\n",
    "    return diags(mu) @ C\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-ef4a737015910fbe",
     "locked": true,
     "points": 2,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# run a finite difference test to see if the Jacobian is correct\n",
    "x = np.random.rand(n)\n",
    "mu = F(x, C)\n",
    "dmu = J(x, C)\n",
    "y = np.random.randn(n)\n",
    "epss = [1.e-4, 5.e-5]\n",
    "rs = []\n",
    "for eps in epss:\n",
    "    xnew = x + eps * y\n",
    "    mupred = mu + eps * dmu @ y\n",
    "    munew = F(xnew, C)\n",
    "    rs.append(np.linalg.norm(mupred - munew) / np.linalg.norm(munew))\n",
    "\n",
    "# estimate how quickly the linear prediction is converging to the truth: should be second order\n",
    "order_est = np.log2(rs[0] / rs[1]) / np.log2(epss[0] / epss[1])\n",
    "print(order_est)\n",
    "assert(order_est > 1.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gauss-Newton\n",
    "\n",
    "The gradient $\\nabla f(x)$ is\n",
    "\n",
    "$$\\nabla f(x) = (F(x) - d)^T J(x).$$\n",
    "\n",
    "The action of the true Hessian $\\nabla^2 f(x)[v]$ is\n",
    "\n",
    "$$\\nabla^2 f(x)[v] = J^T(x) J(x) v + (F(x) - d)^T \\nabla J(x)[v].$$\n",
    "\n",
    "You can see that if $F(x) - d = 0$, then\n",
    "\n",
    "$$\\nabla^2 f(x)[v] = J^T(x) J(x) v.$$\n",
    "\n",
    "The linear operator $J^T(x) J(x)$ is the _Gauss-Newton approximation_ to the Hessian $\\nabla^2 f(x)$.\n",
    "\n",
    "Therefore if there is a value of $x^*$ such that $d = F(x^*)$, then the Gauss-Newton approximation to the Hessian\n",
    "_is_ the Hessian at the solution to the nonlinear least-squares problem.\n",
    "\n",
    "We set up the necessary routines for evaluating $f$, $\\nabla f$, and the Gauss-Newton Hessian approximation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class GaussNewton(LinearOperator):\n",
    "    def __init__(self, x, C):\n",
    "        super(GaussNewton,self).__init__(shape=(C.shape[1],)*2, dtype=C.dtype)\n",
    "        self._GNJ = J(x, C)\n",
    "        \n",
    "    def _matvec(self, v):\n",
    "        return self._GNJ.T @ (self._GNJ @ v)\n",
    "\n",
    "def f(x, d, C):\n",
    "    e = F(x,C) - d\n",
    "    return 0.5 * e.dot(e)\n",
    "\n",
    "def f_grad(x, d, C):\n",
    "    Jx = J(x,C)\n",
    "    e = F(x,C) - d\n",
    "    return Jx.T @ e\n",
    "\n",
    "def f_hess_gauss_newton(x, d, C):\n",
    "    return GaussNewton(x, C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define a standard Newton-CG iteration (note: the Gauss-Newton Hessian is always symmetric positive semi-definite, so we can use CG without the Steihaug termination condition)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def newton_cg(f, f_grad, f_hess, x0, args=(), maxiter=100, gtol=1.e-6, xtol=1.e-12, armijo=1.e-4, lamda_min=1.e-12, verbose=False):\n",
    "    x = x0.copy()\n",
    "    fx = f(x, *args)\n",
    "    gx = f_grad(x, *args)\n",
    "    gxnorm = np.linalg.norm(gx)\n",
    "    rs = []\n",
    "    rs.append(gxnorm)\n",
    "    for i in range(maxiter):\n",
    "        if gxnorm < gtol:\n",
    "            return x, rs\n",
    "        H = f_hess(x, *args)\n",
    "        p = cg(H, -gx, tol=1.e-12)\n",
    "        p = p[0]\n",
    "        phi_grad = p.dot(gx)\n",
    "        assert(phi_grad < 0.)\n",
    "        lamda = 1.\n",
    "        while lamda >= lamda_min:\n",
    "            xnew = x + lamda * p\n",
    "            fxnew = f(xnew, *args)\n",
    "            diff = fxnew - fx\n",
    "            if diff < armijo * lamda * phi_grad:\n",
    "                break\n",
    "            lamda *= 0.5\n",
    "        if np.linalg.norm(lamda*p) < xtol:\n",
    "            return x, rs\n",
    "        fx = fxnew\n",
    "        x = xnew\n",
    "        gx = f_grad(x, *args)\n",
    "        if verbose:\n",
    "            print(lamda, gx.dot(p) / phi_grad)\n",
    "        gxnorm = np.linalg.norm(gx)\n",
    "        rs.append(gxnorm)\n",
    "    return x, np.array(rs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we observe what the convergence is like when the vector $d_{true}$ was generated by some $x_{true}$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "xtrue = 0.25 * np.random.rand(n)\n",
    "dtrue = F(xtrue, C)\n",
    "x, rs = newton_cg(f, f_grad, f_hess_gauss_newton, np.zeros(n), args=(dtrue, C))\n",
    "\n",
    "ax = plt.figure().gca()\n",
    "ax.set(title=\"Newton-CG GN, zero noise\", xlabel=\"iteration k\", ylabel=r'$\\|F(x_k) - d\\|$')\n",
    "_ = ax.semilogy(rs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 2 (2 points):** Now create a plot where $d = d_{true} \\epsilon$, where $\\epsilon$ is random noise with mean 1 and varying standard deviations $\\sigma$.\n",
    "\n",
    "_Hint:_ Gamma($\\sigma^{-2}$, $\\sigma^2$) is one such choice for the random noise.\n",
    "\n",
    "_Hint 2:_ Use a range of $\\sigma$ between 0.01 and 0.5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-03384c09b87cfd8e",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "ax = plt.figure().gca()\n",
    "for std in [2.**k for k in range(-6,0)]:\n",
    "    print('std', std)\n",
    "    noise = np.random.gamma(std**-2,std**2,m)\n",
    "    d = noise * dtrue\n",
    "    x, rs = newton_cg(f, f_grad, f_hess_gauss_newton, 0.1*np.random.rand(n), args=(d, C))\n",
    "    ax.semilogy(rs / rs[0], label='std = {}'.format(std))\n",
    "_ = ax.legend()\n",
    "### END SOLUTION"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
