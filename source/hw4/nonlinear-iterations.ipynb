{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HW 4: Nonlinear iterations\n",
    "\n",
    "9 Points\n",
    "\n",
    "We put together many of the techniques we have learning about to solve a large 3D nonlinear PDE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**collaboration statement:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install scipy numpy matplotlib pyamg\n",
    "import time\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from itertools import product\n",
    "from scipy.optimize import minimize\n",
    "from scipy.sparse import coo_matrix, dia_matrix, kron, eye, csr_matrix, diags, lil_matrix, vstack\n",
    "from scipy.sparse.linalg import LinearOperator, cg\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A nonlinear PDE problem: the Bratu $p$-Laplacian\n",
    "\n",
    "In this homework you will build up the components of a solver for the Bratu $p$-Laplacian nonlinear PDE problem.  The problem can be stated the finding of a field $u$ that minimizes an energy:\n",
    "\n",
    "$$\\min_{u, u|_{\\partial \\Omega} = 0} f(u) := \\int_{\\Omega} (|\\nabla u|^2 + \\epsilon)^{p/2} - b u\\ dx.$$\n",
    "\n",
    "When the small parameter $\\epsilon$ is 0, $(|\\nabla u|^2 + \\epsilon)^{p/2} = |\\nabla u|^p$, so the energy is associated with the $p$th power of the norm of the gradient of $u$, hence the name.\n",
    "\n",
    "The solution $u$ satisfies the nonlinear PDE\n",
    "\n",
    "$$-\\nabla \\cdot \\left\\{\\underbrace{p(|\\nabla u|^2 + \\epsilon)^{(p-2)/2}}_{:= \\mu(\\nabla u)}\\nabla u \\right\\}= b.$$\n",
    "\n",
    "(If $p= 2$, is the linear Poisson problem).\n",
    "\n",
    "Below you will find a `BratuProblem` object that can evaluate\n",
    "\n",
    "- $f(u)$, the objective function,\n",
    "- $\\nabla f(u)$, the gradient of the objective, and\n",
    "- $\\nabla^2 f(u)[v]$, the matrix vector product of the Hessian of the objective with a vector $v$.\n",
    "\n",
    "The quantity $\\nabla^2 f(u)[v]$ looks like the following differential operator applied to $v$:\n",
    "\n",
    "$$-\\nabla \\cdot \\left\\{\\mu(\\nabla u) \\nabla v + \\mu'(u)(\\nabla v  \\cdot \\nabla u) \\nabla u\\right\\}.$$\n",
    "\n",
    "It is easier to compute this Hessian-vector product than it is to build the Hessian matrix.  Instead, if we need to build a matrix for preconditioning, we will build the _Picard_ approximation to the Hessian,\n",
    "\n",
    "$$-\\nabla \\cdot \\left\\{\\mu(\\nabla u) \\nabla v\\right\\}.$$\n",
    "\n",
    "The `BratuProblem` will also construct this matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-3f8fae5b4df6f578",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def grid_interp_matrix(d, n):\n",
    "    B1D = lil_matrix((2*(n+1),n))\n",
    "    eta = (1.-1./3.**0.5) / 2.\n",
    "    for i in range(n):\n",
    "        B1D[2*i + 0,i] = eta\n",
    "        B1D[2*i + 1,i] = 1. - eta\n",
    "        B1D[2*i + 2,i] = 1. - eta\n",
    "        B1D[2*i + 3,i] = eta\n",
    "    B1D = csr_matrix(B1D)\n",
    "    B1D.eliminate_zeros()\n",
    "    B = B1D.copy()\n",
    "    for i in range(d-1):\n",
    "        B = csr_matrix(kron(B,B1D))\n",
    "        B.eliminate_zeros()\n",
    "    return B\n",
    "\n",
    "def grid_grad_matrix(d, n):\n",
    "    \"\"\"Sparse matrix equivalent to evaluating grid_grad\"\"\"\n",
    "    G1D = lil_matrix((2*(n+1),n))\n",
    "    h = 1. / (n + 1.)\n",
    "    for i in range(n):\n",
    "        G1D[2*i + 0, i] = 1./h\n",
    "        G1D[2*i + 1, i] = 1./h\n",
    "        G1D[2*i + 2, i] = -1./h\n",
    "        G1D[2*i + 3, i] = -1./h\n",
    "    G1D = csr_matrix(G1D)\n",
    "    G1D.eliminate_zeros()\n",
    "    if d == 1:\n",
    "        return G1D\n",
    "    G = []\n",
    "    B1D = grid_interp_matrix(1, n)\n",
    "    for i in range(d):\n",
    "        if i == 0:\n",
    "            Gi = G1D.copy()\n",
    "        else:\n",
    "            Gi = B1D.copy()\n",
    "        for j in range(1,d):\n",
    "            if j == i:\n",
    "                Gi = csr_matrix(kron(Gi, G1D))\n",
    "            else:\n",
    "                Gi = csr_matrix(kron(Gi, B1D))\n",
    "            Gi.eliminate_zeros()\n",
    "        G.append(Gi)\n",
    "    G = csr_matrix(vstack(G))\n",
    "    return G"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-0d35a41b27764204",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_rhs(d, n, B):\n",
    "    \"\"\"The rhs b for the nonlinear problem\"\"\"\n",
    "    x1D = np.zeros((2*(n+1)))\n",
    "    eta = (1.-1./3.**0.5) / 2.\n",
    "    h = 1. / (n+1)\n",
    "    for i in range(n+1):\n",
    "        x1D[2*i + 0] = i * h + eta * h\n",
    "        x1D[2*i + 1] = i * h + (1. - eta) * h\n",
    "    x1D -= 0.5\n",
    "    x = np.zeros((d,)+(2*(n+1),)*d)\n",
    "    for i in range(d):\n",
    "        x[i] = x1D[(np.newaxis,)*i + (slice(0,2*(n+1)),) + (np.newaxis,)*(d-1-i)]\n",
    "    x = x.reshape((d,(2*(n+1))**d))\n",
    "    xdot = np.einsum('ij,ij->j', x, x).reshape((2*(n+1),)*d)\n",
    "    b = np.array(xdot < d*0.25**2,dtype=x.dtype)\n",
    "    b = (b - 0.5) * 10\n",
    "    return (B.T @ b.ravel()).reshape((n,)*d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-5514c41c0bf060c9",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_objective(d, n, p, x, G, B, b=None, eps=1.e-3):\n",
    "    \"\"\"Compute the energy for function x\"\"\"\n",
    "    if b is None:\n",
    "        b = bratu_rhs(d, n, B).reshape(x.shape)\n",
    "    Gx = (G @ x).reshape((d,(2*(n+1))**d))\n",
    "    Gdot = np.einsum('ij,ij->j', Gx, Gx)\n",
    "    sigma = (Gdot + eps)**(p/2.)\n",
    "    return np.sum(sigma.ravel()) - x.dot(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-2e7bb1e479fd176a",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_gradient(d, n, p, x, G, GT, B, b=None, eps=1.e-3):\n",
    "    \"\"\"Compute the gradient of the energy for function x\"\"\"\n",
    "    if b is None:\n",
    "        b = bratu_rhs(d, n, B).reshape(x.shape)\n",
    "    Gx = (G @ x).reshape((d,(2*(n+1))**d))\n",
    "    Gdot = np.einsum('ij,ij->j', Gx, Gx).reshape((2*(n+1),)*d)\n",
    "    mu = (p/2.)*(Gdot + eps)**((p - 2.)/2.)\n",
    "    sigma_der = mu[np.newaxis,...]*(2.*Gx.reshape((d,)+(2*(n+1),)*d))\n",
    "    div_sigma = GT @ sigma_der.ravel()\n",
    "    return div_sigma - b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-f0139d38402ee523",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_hessian_vector_product(d, n, p, x, v, G, GT, Gx, mu, mu_der, eps=1.e-3):\n",
    "    \"\"\"Compute the Hessian vector product for function x applied to vector v\"\"\"\n",
    "    Gv = (G @ v).reshape((d,(2*(n+1))**d))\n",
    "    Gvdot = np.einsum('ij,ij->j', Gx, Gv).reshape((2*(n+1),)*d)\n",
    "    sigma_der = mu[np.newaxis,...]*Gv.reshape((d,)+(2*(n+1),)*d)\n",
    "    sigma_der += (mu_der*Gvdot)[np.newaxis,...]*Gx.reshape((d,)+(2*(n+1),)*d)\n",
    "    div_sigma = GT @ sigma_der.ravel()\n",
    "    return div_sigma"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-f5c3aa8e92c4cff1",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_picard_hessian_diagonal(d, n, p, x, G, eps=1.e-3):\n",
    "    \"\"\"Compute mu(grad x), the coefficient in the Picard approximation to the Hessian\"\"\"\n",
    "    Gx = (G @ x).reshape((d,(2*(n+1))**d))\n",
    "    Gdot = np.einsum('ij,ij->j', Gx, Gx).reshape((2*(n+1),)*d)\n",
    "    q = ((p-2.)/2.)\n",
    "    mu = (p/2.)*(Gdot + eps)**q\n",
    "    return 2. * mu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-6c06fa062bf98027",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "class BratuProblem(object):\n",
    "    def __init__(self, d, n, p, b=None, eps=1.e-3):\n",
    "        self.B = grid_interp_matrix(d, n)\n",
    "        self.G = grid_grad_matrix(d, n)\n",
    "        self.GT = csr_matrix(self.G.T)\n",
    "        if b is None:\n",
    "            b = bratu_rhs(d, n, self.B).ravel()\n",
    "        self.d = d\n",
    "        self.n = n\n",
    "        self.p = p\n",
    "        self.b = b\n",
    "        self.eps = eps\n",
    "        self.shape = (b.shape[0], b.shape[0])\n",
    "        \n",
    "    def objective(self, x):\n",
    "        \"\"\"evaluate the objective\"\"\"\n",
    "        return bratu_objective(self.d, self.n, self.p, x, self.G, self.B, self.b, self.eps)\n",
    "    \n",
    "    def gradient(self, x):\n",
    "        \"\"\"evaluate the gradient of the objective\"\"\"\n",
    "        return bratu_gradient(self.d, self.n, self.p, x, self.G, self.GT, self.B, self.b, self.eps)\n",
    "    \n",
    "    def hessian(self, x):\n",
    "        \"\"\"Return a scipy.sparse.linalg.LinearOperator for the Hessian vector product\"\"\"\n",
    "        d = self.d\n",
    "        n = self.n\n",
    "        Gx = (self.G @ x).reshape((d,(2*(n+1))**d))\n",
    "        Gdot = np.einsum('ij,ij->j', Gx, Gx).reshape((2*(n+1),)*d)\n",
    "        q = ((p-2.)/2.)\n",
    "        mu = p*(Gdot + eps)**q\n",
    "        mu_der = (q * mu / (Gdot + eps))*2\n",
    "        def matvec(y):\n",
    "            return bratu_hessian_vector_product(self.d, self.n, self.p, x, y, self.G, self.GT, Gx, mu, mu_der, self.eps)\n",
    "        return LinearOperator(self.shape,matvec=matvec)\n",
    "    \n",
    "    def hessian_picard_matrix(self, x):\n",
    "        \"\"\"Return a scpy.sparse.csr_matrix for the picard approximation of the Hessian\"\"\"\n",
    "        mu = bratu_picard_hessian_diagonal(self.d, self.n, self.p, x, self.G, self.eps).reshape((2*(self.n+1))**self.d)\n",
    "        mu = np.kron(np.ones(self.d), mu)\n",
    "        L = csr_matrix(self.GT @ diags(mu) @ self.G)\n",
    "        return L  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A globalized nonlinear solver: Newton-CG linesearch\n",
    "\n",
    "The Bratu energy is always convex for $p > 1$, so the Hessian $H(u) = \\nabla^2 f(u)$ is always positive definite.\n",
    "Therefore the Newton update direction is always a descent direction, so a Newton iteration globalized by a linesearch will converge to the solution.  An implementation of Newton-CG linesearch is below.\n",
    "\n",
    "The function returns the solution $x$, as well as the residual history.  Two residuals are computed at each _CG iteration_.  When using CG to solve\n",
    "\n",
    "$$\\nabla^2 f(x_k) p = - \\nabla f(x_k),$$\n",
    "\n",
    "it records the linear residual,\n",
    "\n",
    "$$\\|\\nabla^2 f(x_k) p + \\nabla f(x_k)\\|,$$\n",
    "\n",
    "as well as the magnitude of the gradient at the potential new point $x_k + p$,\n",
    "\n",
    "$$\\|\\nabla f(x_k + p)\\|.$$\n",
    "\n",
    "When these two values are similar, it means that the quadratric approximation\n",
    "\n",
    "$$m(x) = f(x_k) + \\nabla f(x_k) \\cdot (x - x_k) + \\tfrac{1}{2} (x - x_k)^T \\nabla^2 f(x_k) (x - x_k),$$\n",
    "\n",
    "is a good approximation for $f(x)$.  When they are different, it means that solving the Newton update with high accuracy is not very useful.  Each residual evaluation also comes with a timestamp, so that we can track convergence in terms of runtime, not iteration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-a037f145974f882e",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def newton_cg(f, f_der, f_hess, x0,\n",
    "              cg_tol_0=1.e-3, cgupdate=None,\n",
    "              f_hess_prec=None, f_hess_prec_update=None,\n",
    "              tol=1.e-6, xtol=1.e-10, maxit=100, armijo_alpha=1.e-4, lamda_min=1.e-12):\n",
    "    \"\"\"Minimize f(x) from initial guess x0\n",
    "    \n",
    "    f: function to minize\n",
    "    f_def: gradient of f, returns a vector\n",
    "    f_hess: Hessian of f, returns a matrix or LinearOperator\n",
    "    x0: initial guess\n",
    "    \n",
    "    cg_tol_0: the relative tolerance to use when solving the first Newton update problem\n",
    "    cgupdate: function that determines what tolerance to use for CG iterations after the first, cg_tol_0 is used\n",
    "              if cgupdate is None. The input arguments to cgupdate are:\n",
    "                  gxnorm: the norm of $grad f$ for the current value of x\n",
    "                  prevgxnorm: the norm of $grad f$ for the previous value of x\n",
    "                  prev_linear_rnorm: the residual norm from the previous Newton update problem\n",
    "              \n",
    "    f_hess_prec: the initial preconditioner to use for the first Newton updateproblem\n",
    "    f_hess_prec_update: function that computes a new preconditioner for each Newton update after the first.\n",
    "                        the initial preconditioner is used if f_hess_prec_update is None.  The input\n",
    "                        arguments to f_hess_prec_update are:\n",
    "                            x: the current solution vector x\n",
    "                            Mx_prev: the preconditioner from the previous iteration\n",
    "                            y: the difference (grad f(x) - grad f(x_prev))\n",
    "                            s: the difference (x - x_prev)\n",
    "                            \n",
    "    tol: the tolerance for |grad f(x)| to end the iteration\n",
    "    xtol: the tolerance for p.dot(grad fx) to end the iteration, where p is the Newton search direction\n",
    "    maxit: the maximum number of iterations\n",
    "    \n",
    "    armijo_alpha: the tolerance for the first armijo line search condition\n",
    "    lambda_min: the minimum line search distance before the line search terminates\n",
    "    \"\"\"\n",
    "    x = x0.copy()\n",
    "    residuals_and_times = []\n",
    "    gxnorm = 0.\n",
    "    M0 = f_hess_prec\n",
    "    gx = None\n",
    "    for i in range(maxit):\n",
    "        # compute the objective and gradient\n",
    "        fx = f(x)\n",
    "        gx_prev = gx\n",
    "        gx = f_der(x)\n",
    "        if i > 0:\n",
    "            y = gx - gx_prev\n",
    "        \n",
    "        # store the previous gradient norm and compute the new one\n",
    "        prevgxnorm = gxnorm\n",
    "        gxnorm = np.linalg.norm(gx)\n",
    "        \n",
    "        # stop if the gradient is small enough\n",
    "        if (gxnorm < tol):\n",
    "            print(\"iter\", i, \"gradient norm\", gxnorm)\n",
    "            break\n",
    "            \n",
    "        # \"Compute\" the Hessian (create the Hessian-vector product object)\n",
    "        Hx = f_hess(x)\n",
    "        \n",
    "        # callback to store the residual of the linear problem and the nonlinear problem at each  CG iteration\n",
    "        cgit = [0]\n",
    "        def cg_callback(xk):\n",
    "            linear_residual = np.linalg.norm(Hx @ xk + gx)\n",
    "            nonlinear_residual = np.linalg.norm(f_der(x + xk))\n",
    "            cgit[0] += 1\n",
    "            residuals_and_times.append([time.time(), linear_residual, nonlinear_residual])\n",
    "            \n",
    "        # Compute the preconditioner\n",
    "        if i == 0:\n",
    "            Mx = M0\n",
    "        elif f_hess_prec_update is not None:\n",
    "            Mx_prev = Mx\n",
    "            Mx = f_hess_prec_update(x, Mx_prev, y, s)\n",
    "            \n",
    "        # Determine the tolerance for CG\n",
    "        if i == 0 or cgupdate is None:\n",
    "            cg_tol = cg_tol_0\n",
    "        else:\n",
    "            cg_tol = cgupdate(gxnorm, prevgxnorm, prev_linear_rnorm, tol)\n",
    "            \n",
    "        # Compute the Newton update direction\n",
    "        sol = cg(Hx, -gx, M=Mx, callback=cg_callback, atol=0., tol=cg_tol)\n",
    "        p = sol[0]\n",
    "\n",
    "        lamda = 1. \n",
    "        pgx = p.dot(gx)\n",
    "        if (np.abs(pgx) < xtol):\n",
    "            print(\"iter\", i, \"cg iterations\", cgit[0], \"gradient norm\", gxnorm, \"step norm\", pgx)\n",
    "            break\n",
    "        while lamda > lamda_min:\n",
    "            xnew = x + lamda * p\n",
    "            fxnew = f(xnew)\n",
    "            diff = fxnew - fx\n",
    "            if diff < armijo_alpha * pgx:\n",
    "                break\n",
    "            lamda = lamda / 2.\n",
    "        if (lamda <= lamda_min):\n",
    "            break\n",
    "        prev_linear_rnorm = np.linalg.norm(lamda * Hx @ p + gx)\n",
    "        print(\"iter\", i, \"cg iterations\", cgit[0], \"gradient norm\", gxnorm, \"step norm\", pgx, \"line search length\", lamda)\n",
    "        s = xnew - x\n",
    "        x = xnew\n",
    "    return x, residuals_and_times\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of using the `newton_cg` solver on the Bratu problem in 2D for different problem sizes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-3b4126790b2984f8",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "d = 3\n",
    "p = 1.5\n",
    "eps=1.e-8\n",
    "\n",
    "for n in [3, 7, 15, 31,]:\n",
    "    print('------ n = {} ------'.format(n))\n",
    "    t0 = time.time()\n",
    "    prob = BratuProblem(d, n, p, eps=eps)\n",
    "    x0 = np.zeros(prob.shape[0])\n",
    "    x, res = newton_cg(prob.objective, prob.gradient, prob.hessian, x0)\n",
    "    res = np.array(res)\n",
    "    ax = plt.figure(figsize=(8,5)).gca()\n",
    "    ax.set(title=\"{}D Bratu, p={}, n={}\".format(d,p,n), xlabel='time (seconds)', ylabel=r'residual / gradient norm')\n",
    "    ax.semilogy(res[:,0]-t0,res[:,1], 'x--', label=\"linear residual\")\n",
    "    ax.semilogy(res[:,0]-t0,res[:,2], label=r'$\\nabla f$')\n",
    "    ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 1 (2 points):** Looking at the convergence plots, we see that in solving the linear equations that CG takes a lot more iterations to converge as $n$ increases.  This is because CG is not preconditioned right now.  The Hessian is like a variable coefficient Laplacian, so a multilevel preconditioner (like multigrid or multilevel domain decomposition) could get us close to $h$-independent convergence.\n",
    "\n",
    "Rather than writing your own multilevel preconditioner, use the [pyamg](pyamg.readthedocs.io) library to construct a preconditioner for $\\nabla^2 f(x)$ from the matrix of the Picard approximation.\n",
    "\n",
    "_Hint:_ Take a look at [this demo](https://github.com/pyamg/pyamg-examples/blob/master/Preconditioning/demo.py) to see how a preconditioner can be constructed using `pyamg`.\n",
    "\n",
    "_Hint 2:_ `pyamg` provides two relevant algebraic multigrid preconditioners, one \"classical Ruge-Stuben\" and one \"smoothed aggregation\".  Try them both and use the one that gives you the best runtimes for this problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-28cd37fbbc9af367",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def bratu_picard_preconditioner(bratu_prob, x):\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        bratu_prob (BratuProblem): driver for the nonlinear equations\n",
    "        x (np.ndarray): the current solution\n",
    "    Returns:\n",
    "        scipy.sparse.linalg.LinearOperator: a multigrid preconditioner based on the Picard approximation to\n",
    "        the Hessian\"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    from pyamg import smoothed_aggregation_solver, ruge_stuben_solver\n",
    "    H = bratu_prob.hessian_picard_matrix(x)\n",
    "    M = smoothed_aggregation_solver(H).aspreconditioner()\n",
    "    return M\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now use your preconditioner in `newton_cg`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c6ec4fd589dcd093",
     "locked": true,
     "points": 2,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "d = 3\n",
    "p = 1.5\n",
    "eps=1.e-8\n",
    "\n",
    "for n in [3, 7, 15, 31,]:\n",
    "    print('------ n = {} ------'.format(n))\n",
    "    t0 = time.time()\n",
    "    prob = BratuProblem(d, n, p, eps=eps)\n",
    "    def preconditioner_update_picard(x, Mprev, y, s):\n",
    "        return bratu_picard_preconditioner(prob, x)\n",
    "    x0 = np.zeros(prob.shape[0])\n",
    "    M0 = bratu_picard_preconditioner(prob, x0)\n",
    "    x, res = newton_cg(prob.objective, prob.gradient, prob.hessian, x0, f_hess_prec=M0,\n",
    "                       f_hess_prec_update=preconditioner_update_picard)\n",
    "    res = np.array(res)\n",
    "    ax = plt.figure(figsize=(8,5)).gca()\n",
    "    ax.set(title=\"{}D Bratu, p={}, n={}\".format(d,p,n), xlabel='time (seconds)', ylabel=r'residual / gradient norm')\n",
    "    ax.semilogy(res[:,0]-t0,res[:,1], 'x--', label=\"linear residual\")\n",
    "    ax.semilogy(res[:,0]-t0,res[:,2], label=r'$\\nabla f$')\n",
    "    ax.legend()\n",
    "    ### BEGIN HIDDEN TESTS\n",
    "    assert(len(res) < 40)\n",
    "    ### END HIDDEN TESTS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2 (2 points):** We see that the magnitude of the gradient (orange) diverges from the linear residual (blue).  This means that we are oversolving the Newton updates, and we should use a coarser tolerance.  Use the Eisenstat-Walker method (equation 2.2 in the the paper in the course notes) to choose the relative tolerance based on:\n",
    "\n",
    "- `current_nonlin_res`: $\\|\\nabla f(x_k)\\|$\n",
    "- `prev_nonlin_res`: $\\|\\nabla f(x_{k-1})\\|$\n",
    "- `prev_linl_res`: $\\|\\nabla^2 f(x_{k-1})(x_k - x_{k-1}) + \\nabla f(x_{k-1})\\|$\n",
    "- `target_nonlin_res`: The desired tolerance for $\\|\\nabla f(x_{k+1})\\|$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-b3c78e95150b60c9",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def cgupdate_eisenstat_walker(curr_nonlin_res, prev_nonlin_res, prev_lin_res, target_nonlin_res):\n",
    "    ### BEGIN SOLUTION\n",
    "    cg_tol_max = 0.1\n",
    "    eta = np.abs(curr_nonlin_res - prev_lin_res) / prev_nonlin_res\n",
    "    eta = min(cg_tol_max, eta)\n",
    "    return max(eta, 0.1 * target_nonlin_res / curr_nonlin_res)\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-8495665f3f9d0ffc",
     "locked": true,
     "points": 2,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "d = 3\n",
    "p = 1.5\n",
    "eps=1.e-8\n",
    "\n",
    "for n in [3, 7, 15, 31,]:\n",
    "    print('------ n = {} ------'.format(n))\n",
    "    t0 = time.time()\n",
    "    prob = BratuProblem(d, n, p, eps=eps)\n",
    "    def preconditioner_update_picard(x, Mprev, y, s):\n",
    "        return bratu_picard_preconditioner(prob, x)\n",
    "    x0 = np.zeros(prob.shape[0])\n",
    "    M0 = bratu_picard_preconditioner(prob, x0)\n",
    "    x, res = newton_cg(prob.objective, prob.gradient, prob.hessian, x0, f_hess_prec=M0,\n",
    "                       f_hess_prec_update=preconditioner_update_picard,\n",
    "                       cgupdate=cgupdate_eisenstat_walker)\n",
    "    res = np.array(res)\n",
    "    ax = plt.figure(figsize=(8,5)).gca()\n",
    "    ax.set(title=\"2D Bratu, p={}, n={}\".format(d,n), xlabel='time (seconds)', ylabel=r'residual / gradient norm')\n",
    "    ax.semilogy(res[:,0]-t0,res[:,1], 'x--', label=\"linear residual\")\n",
    "    ax.semilogy(res[:,0]-t0,res[:,2], label=r'$\\nabla f$')\n",
    "    ax.legend()\n",
    "    ### BEGIN HIDDEN TESTS\n",
    "    assert(len(res) < 30)\n",
    "    ### END HIDDEN TESTS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3 (2 points):** Recomputing the preconditioner gives us good convergence rates for each CG solve, but it takes time.  One thing we would like to be able to do is reuse the preconditioner from a previous iteration.\n",
    "\n",
    "A way to take a preconditioner from a previous iteration and make t more accurate for the current Newton update problem is to use a secant method.  So if I have a preconditioner $M_k \\approx (\\nabla f^2 (x_k))$, I can perform the BFGS update\n",
    "\n",
    "$$M_{k+1} = M_k + \\frac{y y^T}{y^T s} - \\frac{M_k s s^T M_k}{s^T M_k s},$$\n",
    "\n",
    "where $s = x_{k+1} - x_{k}$ and $y = \\nabla f(x_{k+1}) - \\nabla f(x)$.\n",
    "\n",
    "Below, the class `BFGSUpdate` takes in a precontioner `Mprev` that is a `scipy.sparse.linalg.LinearOperator` for the previous preconditioner, the current solution `x`, and the vectors `y` and `s` and will store information necessary to apply the inverse of the BFGS update of `Mprev`.  The class will also recompute the preconditioner from scratch every `maxlevel` iterations.\n",
    "\n",
    "The values of $M_{k}^{-1} y$, $y^T s$, $d = s - M_{k}^{-1} y$ and $d^T y$ are stored for you.  Use them to compute\n",
    "$M_{k+1}^{-1} v$.\n",
    "\n",
    "_Hint:_ use the formula for the inverse of a rank-2 update from Dennis & Schnabel, chapter 9."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-706f4a400569d0c0",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "class BFGSUpdate(LinearOperator):\n",
    "    def __init__(self, x, Mprev, y, s, maxlevel=2, recompute=None):\n",
    "        super(BFGSUpdate, self).__init__(shape=Mprev.shape, dtype=Mprev.dtype)\n",
    "        if isinstance(Mprev, BFGSUpdate):\n",
    "            self._bfgslevel = Mprev._bfgslevel + 1\n",
    "        else:\n",
    "            self._bfgslevel = 0\n",
    "        if self._bfgslevel == maxlevel and recompute:\n",
    "            Mprev = recompute(x, None, None, None)\n",
    "            self._bfgslevel = -1\n",
    "            y = np.zeros(y.shape)\n",
    "            s = np.zeros(s.shape)\n",
    "            \n",
    "        # Here are the stored values that you will need in computing the matvec\n",
    "        self.Mprev = Mprev\n",
    "        self.Mprevy = Mprev @ y\n",
    "        self.ys = y.dot(s)\n",
    "        assert(self.ys >= 0.)\n",
    "        self.s = s\n",
    "        self.y = y\n",
    "        self.d = s - self.Mprevy\n",
    "        self.dy = self.d.dot(self.y)\n",
    "\n",
    "        \n",
    "    def _matvec(self, v):\n",
    "        if self._bfgslevel == -1:\n",
    "            return self.Mprev @ v\n",
    "        \"\"\"Apply the BFGS update to Mprev to the vector here\"\"\"\n",
    "        ### BEGIN SOLUTION\n",
    "        s = self.s\n",
    "        y = self.y\n",
    "        d = self.d\n",
    "        Mprev = self.Mprev\n",
    "        Mprevy = self.Mprevy\n",
    "        ys = self.ys\n",
    "        dy = self.dy\n",
    "        sv = s.dot(v)\n",
    "        dv = d.dot(v)\n",
    "        Mprevv = Mprev @ v\n",
    "        return Mprevv + (d * sv + s * dv) / ys - (s * dy * sv) / (ys**2)\n",
    "        ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-44d3f056a040a48c",
     "locked": true,
     "points": 1,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# Test BFGS: create an s.p.d. matrix, and verify that if the inverse is applied to y, it yields s\n",
    "A = np.random.randn(10,10)\n",
    "B = A @ A.T\n",
    "Binv = np.linalg.inv(B)\n",
    "s = np.random.randn(10)\n",
    "y = np.random.randn(10)\n",
    "x = np.random.randn(10)\n",
    "if (y.dot(s) < 0):\n",
    "    y = -y\n",
    "    \n",
    "bfgs = BFGSUpdate(x, Binv, y, s)\n",
    "\n",
    "z = bfgs @ y\n",
    "assert(np.linalg.norm(z - s) < 1.e-8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your BFGS updated preconditioner is used below.  Adjust the `maxlevel` parameter to find the value that\n",
    "minimizes the solution time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-d689bc9171e6526a",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "maxlevel = 0\n",
    "### BEGIN SOLUTION\n",
    "maxlevel = 2\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-3fdb532554b14df7",
     "locked": true,
     "points": 1,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "d = 3\n",
    "p = 1.5\n",
    "eps=1.e-8\n",
    "\n",
    "\n",
    "for n in [3, 7, 15, 31, 63]:\n",
    "    print('------ n = {} ------'.format(n))\n",
    "    t0 = time.time()\n",
    "    prob = BratuProblem(d, n, p, eps=eps)\n",
    "    def preconditioner_update_picard(x, Mprev, y, s):\n",
    "        return bratu_picard_preconditioner(prob, x)\n",
    "    x0 = np.zeros(prob.shape[0])\n",
    "    M0 = bratu_picard_preconditioner(prob, x0)\n",
    "    bfgs = lambda x, Mprev, y, s: BFGSUpdate(x, Mprev, y, s, maxlevel=maxlevel,\n",
    "                                             recompute=preconditioner_update_picard)\n",
    "    x, res = newton_cg(prob.objective, prob.gradient, prob.hessian, x0, f_hess_prec=M0,\n",
    "                       f_hess_prec_update=bfgs,\n",
    "                       cgupdate=cgupdate_eisenstat_walker)\n",
    "    res = np.array(res)\n",
    "    ax = plt.figure(figsize=(8,5)).gca()\n",
    "    ax.set(title=\"{}D Bratu, p={}, n={}\".format(d,p,n), xlabel='time (seconds)', ylabel=r'residual / gradient norm')\n",
    "    ax.semilogy(res[:,0]-t0,res[:,1], 'x--', label=\"linear residual\")\n",
    "    ax.semilogy(res[:,0]-t0,res[:,2], label=r'$\\nabla f$')\n",
    "    ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4 (3 points):** As you can see in the last plot for $n=63$, a lot of time is still spent in the regime before superlinear convergence is observed.  To avoid this, we would need a better initial guess than $x_0 = 0$.\n",
    "\n",
    "We can get a better initial guess by using the solution from a coarser mesh that has been prolonged to the current mesh.  This multigrid-like approach to nonlinear problems is called _grid continuation_.  Define a prolongation operator that extends a coarse grid with grid spacing $H$ to a fine grid with $h=H/2$.\n",
    "\n",
    "_Hint 1:_ You could use the prolongation operator from activity 3.3, but the prolongation of the coarse solution will be closer to the true solution if a higher-order operator is used.  You may want to use a prolongation based on the 1D stencil:\n",
    "\n",
    "$$p = ] 1/8 \\ 1/2 \\ 3/4 \\ 1/2 \\ 1/8 [.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-7687e4c423ea9018",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def prolongation(d, n_H, x_H):\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        d (int): the dimension\n",
    "        n_H (int): the number of gridpoints per direction\n",
    "        x_H (ndarray): a d-dimension array with n points in each direction\n",
    "    Returns:\n",
    "        ndarray: the prolongation of x_H to x_h, a grid with n_h = 2*n_H + 1 points per direction\"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    n_h = 2 * n_H + 1\n",
    "    P = lil_matrix((n_h, n_H))\n",
    "    if True: \n",
    "        # piecewise quadratic interpolation\n",
    "        P[0,0] = 0.5\n",
    "        P[1,0] = 0.75\n",
    "        P[2,0] = 0.5\n",
    "        P[3,0] = 0.125\n",
    "        P[-1,-1] = 0.5\n",
    "        P[-2,-1] = 0.75\n",
    "        P[-3,-1] = 0.5\n",
    "        P[-4,-1] = 0.125\n",
    "        for i in range(1,n_H-1):\n",
    "            P[2*i - 1, i] = 0.125\n",
    "            P[2*i + 0, i] = 0.5\n",
    "            P[2*i + 1, i] = 0.75\n",
    "            P[2*i + 2, i] = 0.5\n",
    "            P[2*i + 3, i] = 0.125\n",
    "    else:\n",
    "        # piecewise linear interpolation\n",
    "        for i in range(n_H):\n",
    "            P[2*i + 0, i] = 0.5\n",
    "            P[2*i + 1, i] = 1.0\n",
    "            P[2*i + 2, i] = 0.5\n",
    "    P = csr_matrix(P)\n",
    "    x_H = x_H.ravel()\n",
    "    for i in range(d):\n",
    "        x_H = x_H.reshape((n_H**(d-i-1),n_H,n_h**i))\n",
    "        x_h = np.zeros((n_H**(d-i-1),n_h,n_h**i))\n",
    "        for j in range(n_H**(d-i-1)):\n",
    "            x_h[j] = P @ x_H[j]\n",
    "        x_H = x_h\n",
    "    return x_h.reshape((n_h,)*d)\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we put together all of these ideas into a nonlinear solver for a large 3D problem.  Choose parameters to solve the case $n=63$ as quickly as possible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-5b44a7b52d289368",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "cg_tol_0 = 1.e-3\n",
    "maxlevel = 0\n",
    "### BEGIN SOLUTION\n",
    "cg_tol_0 = 1.e-1\n",
    "maxlevel = 6\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c17162937bc4fec5",
     "locked": true,
     "points": 3,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "d = 3\n",
    "p = 1.5\n",
    "eps=1.e-8\n",
    "\n",
    "x = None\n",
    "ax = plt.figure(figsize=(13,8)).gca()\n",
    "ax.set(title=\"{}D Bratu, p={}\".format(d,p), xlabel='time (seconds)', ylabel=r'residual / gradient norm')\n",
    "t0 = time.time()\n",
    "for n in [3, 7, 15, 31, 63]:\n",
    "    print('------ n = {} ------'.format(n))\n",
    "    prob = BratuProblem(d, n, p, eps=eps)\n",
    "    def preconditioner_update_picard(x, Mprev, y, s):\n",
    "        return bratu_picard_preconditioner(prob, x)\n",
    "    if x is not None:\n",
    "        x0 = prolongation(d, (n-1)//2, x).ravel()\n",
    "    else:\n",
    "        x0 = np.zeros(prob.b.shape)\n",
    "    M0 = bratu_picard_preconditioner(prob, x0)\n",
    "    bfgs = lambda x, Mprev, y, s: BFGSUpdate(x, Mprev, y, s, maxlevel=maxlevel,\n",
    "                                             recompute=preconditioner_update_picard)\n",
    "    x, res = newton_cg(prob.objective, prob.gradient, prob.hessian, x0, f_hess_prec=M0,\n",
    "                       f_hess_prec_update=bfgs,\n",
    "                       cg_tol_0=cg_tol_0,\n",
    "                       cgupdate=cgupdate_eisenstat_walker)\n",
    "    res = np.array(res)\n",
    "    ax.semilogy(res[:,0]-t0,res[:,1], 'x--', label=\"linear residual, n={}\".format(n))\n",
    "    ax.semilogy(res[:,0]-t0,res[:,2], label=r'nonlin residual, n={}'.format(n))\n",
    "    ### BEGIN HIDDEN TESTS\n",
    "    if (n == 63):\n",
    "        assert(len(res) < 50)\n",
    "    ### END HIDDEN TESTS\n",
    "_ = ax.legend()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
