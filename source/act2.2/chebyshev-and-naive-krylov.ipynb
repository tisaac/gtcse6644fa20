{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activity 2.1 & 2.2: Chebyshev polynomials and Krylov subspaces\n",
    "\n",
    "6 points\n",
    "\n",
    "In the first half, we will see the Chebyshev iteration's good properties, but the important thing we are going to observe is **how errors in eigenvalue estimates affect the Chebyshev iteration**.\n",
    "\n",
    "In the second half, we will naively use Krylov subspaces to solve systems of equations: the failure of our naive approach will demonstrate the importance of **well-conditioned bases for Krylov subspaces.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Collaboration statement:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-42e723ad91a9e88b",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%pip install numpy scipy matplotlib\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.sparse import diags\n",
    "from scipy.linalg import solve_triangular"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background: Chebyshev polynomials"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two useful ways of defining the Chebyshev polynomials.  One is by the recursion rule\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "C_0(z) &= 1 \\\\\n",
    "C_1(z) &= z \\\\\n",
    "C_k(z) &= 2 z C_{k-1}(z) - C_{k-2}(z). \\\\\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "(This three term recurrence is common to polynomials that are orthogonal with respect to some measure.  The Chebyshev polynomials are orthogonal in that\n",
    "\n",
    "$$\\int_{-1}^1 C_j(z) C_k(z)(1 - z^2)^{-1/2} dz = \\delta_{jk}\\begin{cases} \\pi & j = k = 0, \\\\ \\frac{pi}{2} & j = k > 0.\\end{cases}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def chebyshev(n, z):\n",
    "    \"\"\"Evaluate all Chebyshev polynomials up to degree n at the point(s) z\"\"\"\n",
    "    assert(n >= 0)\n",
    "    C = np.zeros((n+1,) + z.shape)\n",
    "    C[0,...] = 1.\n",
    "    if n >= 1:\n",
    "        C[1,...] = z\n",
    "    for j in range(2,n+1):\n",
    "        C[j,...] = 2 * z * C[j-1,...] - C[j-2]\n",
    "    return C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = np.linspace(-1.,1.,200)\n",
    "n = 5\n",
    "C = chebyshev(n, z)\n",
    "for i in range(0,n+1):\n",
    "    plt.plot(z,C[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows that in $[-1,-1]$ $\\|C_n\\|_{\\infty} = 1$ for every $n$.  The functions also look sinusoidal, which is literally true under a change of coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = np.arccos(z)\n",
    "for i in range(0,n+1):\n",
    "    plt.plot(y,C[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows a way to compute them non-recurrently _(in the interval $[-1,1]$)_:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def chebyshev_single(n, z):\n",
    "    \"\"\"Evaluate C_n in the interval [-1,1]\"\"\"\n",
    "    assert (all(z >= -1 and z <= 1))\n",
    "    return np.cos(n * np.arccos(z))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "C_n = chebyshev_single(n, z)\n",
    "plt.plot(C[n],'-')\n",
    "plt.plot(C_n, 'x')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For all their bounded stability in the interval $[-1,1]$, here is how they behave slightly outside the interval:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zext = np.linspace(-1.1,1.1,200)\n",
    "Cext = chebyshev(n, zext)\n",
    "for i in range(0,n+1):\n",
    "    plt.plot(Cext[i])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-0871cee7139d68b8",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def chebyshev_iteration(A, lambda_min, lambda_max, b, x0, k):\n",
    "    \"\"\"Record the residual history of k iterates of the Chebyshev iterative method\"\"\"\n",
    "    theta = (lambda_max + lambda_min) / 2 # midpoint,  want to map it to zero\n",
    "    delta = (lambda_max - lambda_min) / 2 # halfwidth, want to map theta plus this to one\n",
    "    # if we map theta to 0 and theta + delta to 1, what does 0 map to?  Call it gamma\n",
    "    gamma = -theta / delta\n",
    "    r = b - A @ x0\n",
    "    R = [r.copy()]\n",
    "    x = x0.copy()\n",
    "    # We want the residual polynomial after k iterations to be p_k(z) = C_k((z - theta)/ delta) / C_k(gamma)\n",
    "    # r_k = r_0 - A x_k, which means that if q_k is the polynomial for x_k, then it must be that\n",
    "    #\n",
    "    # C_k((z-theta)/delta) / C_k(gamma) = 1 - z q_k(z),\n",
    "    #\n",
    "    # Let g_k = C_k(gamma)\n",
    "    g0 = 1\n",
    "    g1 = gk = gamma\n",
    "    # Let sk = g_{k-1} / g_k\n",
    "    sk = 1./gk\n",
    "    #\n",
    "    # so g_k z q_k(z) = g_k - C_k((z-theta)/delta)\n",
    "    #\n",
    "    # if k = 1, this is\n",
    "    #\n",
    "    # z q_1(z) = 1 - (z - theta) / (-theta) = 1 + z / theta - 1 = z / theta\n",
    "    #\n",
    "    # so q_1(z) = 1 / theta = p_0(z) / theta\n",
    "    #\n",
    "    # which implies the first update should be\n",
    "    # x_1 = x_0 + r_0 / theta\n",
    "    d = r / theta\n",
    "    xprev = np.zeros(x.shape)\n",
    "    for i in range(k):\n",
    "        xprev[:] = x[:]\n",
    "        x += d\n",
    "        r = b - A @ x\n",
    "        R.append(r.copy())\n",
    "        # if k > 1, use the recurrence C_k(z) = 2 z C_{k-1} - C_{k-2}\n",
    "        # which implies C_k((z-theta)/delta) = 2 ((z-theta)/delta) C_{k-1}((z-theta)/delta) - C_{k-2}((z-theta)/delta)\n",
    "        #\n",
    "        # replace C_i((z-theta)/delta) with g_i p_i(z) \n",
    "        #\n",
    "        # g_k z q_k(z) = g_k - 2((z-theta)/delta) g_{k-1} p_{k-1}(z) + g_{k-2} p_{k-2}(z)\n",
    "        #              =  g_k\n",
    "        #                -2 (z/delta) g_{k-1} p_{k-1}(z)\n",
    "        #                -2 gamma g_{k-1} p_{k-1}(z) \n",
    "        #                +g_{k-2} p_{k-2}(z)\n",
    "        #              =  g_k\n",
    "        #                -2 (z/delta) g_{k-1} p_{k-1}(z)\n",
    "        #                -(2 gamma g_{k-1} - g_{k-2})p_{k-1}(z)\n",
    "        #                +g_{k-2} (p_{k-2}(z) - p_{k-1}(z))\n",
    "        #              =  g_k\n",
    "        #                -2 (z/delta) g_{k-1} p_{k-1}(z)\n",
    "        #                -g_k p_{k-1}(z)\n",
    "        #                +g_{k-2} (p_{k-2}(z) - p_{k-1}(z))\n",
    "        #              =  g_k (1 - p_{k-1}(z))\n",
    "        #                -2 (z/delta) g_{k-1} p_{k-1}(z)\n",
    "        #                +g_{k-2} ((1 - p_{k-1}(z)) - (1 - p_{k-2}(z)))\n",
    "        #\n",
    "        # now replace 1 - p_i(z) = z q_i(z)\n",
    "        #\n",
    "        # g_k z q_k(z) =  g_k z q_{k-1}(z)\n",
    "        #                -2 (z/delta) g_{k-1} p_{k-1}(z)\n",
    "        #                +g_{k-2} z (q_{k-1}(z) - q_{k-2}(z))\n",
    "        #\n",
    "        # so\n",
    "        #\n",
    "        # q_k(z) = q_{k-1}(z)\n",
    "        #          - (2/delta) s_k p_{k-1}(z) \n",
    "        #          + s_k s_{k-1} (q_{k-1}(z) - q_{k-2}(z))\n",
    "        #\n",
    "        # which equates to the update\n",
    "        #\n",
    "        # x_k = x_{k-1} - (2/delta) s_k r_{k-1}\n",
    "        #       + s_k s_{k-1} (x_{k-1} - x_{k-2})\n",
    "        #\n",
    "        # because of the recurrence g_k = 2 (-theta/delta) g_{k-1} - g_{k-2}\n",
    "        # we have 1/s_k = (2 (-theta/delta) - s_{k-1})\n",
    "        skold = sk\n",
    "        sk = 1./(2 * gamma - sk)\n",
    "        d = -2 * (sk / delta) * r + sk * skold * (x - xprev)\n",
    "    return x, R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a diagonal SPD matrix with eigenvalues evenly distributed between $\\lambda_{\\min}$ and $\\lambda_{\\max}$ and an initial residual vector of all ones.\n",
    "\n",
    "This would mean that each $e_i$ is an eigenvector for $\\lambda_i$, which means that $r^{(k)}_i$ should be\n",
    "the evaluation of the $k$th residual polynomial at the value $\\lambda_i$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-fb18bb9e45d3b6d5",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "n = 100\n",
    "lambda_min = 0.1\n",
    "lambda_max = 1.\n",
    "lambdas = np.linspace(lambda_min,lambda_max,n)\n",
    "A = diags(lambdas)\n",
    "x0 = np.zeros(n)\n",
    "b = np.ones(n)\n",
    "x, rs = chebyshev_iteration(A, lambda_min, lambda_max, b, x0, 5)\n",
    "ax = plt.figure(figsize=(8,5)).gca()\n",
    "ax.set(title=\"Components of the Chebyshev iteration residual\")\n",
    "ax.set(xlabel=r'$\\lambda_i$')\n",
    "ax.set(ylabel=r'$r^{(k)}_i$')\n",
    "for i in range(6):\n",
    "    ax.plot(lambdas, rs[i], 'x', label='$r^{}$'.format(i))\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For comparison, let's also return the residuals of the Richardson iteration with the optimal choice of $\\alpha$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-38a7b47cee872d08",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def richardson_iteration(A, lambda_min, lambda_max, b, x0, k):\n",
    "    \"\"\"Record the residuals of k iterates of the Richardson iterative method\"\"\"\n",
    "    alpha = 2. / (lambda_min + lambda_max)\n",
    "    r = b - A @ x0\n",
    "    R = [r.copy()]\n",
    "    x = x0.copy()\n",
    "    for i in range(k):\n",
    "        x += alpha * r\n",
    "        r = b - A @ x\n",
    "        R.append(r.copy())\n",
    "    return x, R"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-65566e98364ecb48",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "xrich, rsrich = richardson_iteration(A, lambda_min, lambda_max, b, x0, 5)\n",
    "ax = plt.figure(figsize=(8,5)).gca()\n",
    "ax.set(title=\"Components of the Richardson iteration residual\")\n",
    "ax.set(xlabel=r'$\\lambda_i$')\n",
    "ax.set(ylabel=r'$r^{(k)}_i$')\n",
    "for i in range(6):\n",
    "    ax.plot(lambdas, rsrich[i], '+', label='$r^{}$'.format(i))\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's plot the $\\infty$-norms of the residuals of the Richardson and Chebyshev iterations, along\n",
    "with the convergence factors predicted by the theory,\n",
    "\n",
    "$$\\rho_{\\text{Richardson}} = \\frac{\\frac{\\lambda_{\\max}}{\\lambda_{\\min}} - 1}{\\frac{\\lambda_{\\max}}{\\lambda_{\\min}} + 1}$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\\rho_{\\text{Chebyshev}} = \\frac{\\sqrt{\\frac{\\lambda_{\\max}}{\\lambda_{\\min}}} - 1}{\\sqrt{\\frac{\\lambda_{\\max}}{\\lambda_{\\min}}} + 1}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xcheb, rscheb = chebyshev_iteration(A, lambda_min, lambda_max, b, x0, 20)\n",
    "xrich, rsrich = richardson_iteration(A, lambda_min, lambda_max, b, x0, 20)\n",
    "chebresnorms = [np.max(np.abs(r)) for r  in rscheb]\n",
    "richresnorms = [np.max(np.abs(r)) for r  in rsrich]\n",
    "ax = plt.figure(figsize=(8,5)).gca()\n",
    "ax.set(title=\"Infinity norms of the residuals\")\n",
    "ax.set(xlabel=\"iterate k\")\n",
    "ax.set(ylabel=r'$\\|r^{(k)}\\|_\\infty$')\n",
    "iterates = list(range(21))\n",
    "ax.semilogy(iterates,chebresnorms,label=\"Chebyshev iteration\")\n",
    "ax.semilogy(iterates,richresnorms,label=\"Richardson iteration\")\n",
    "eigratio = lambda_max / lambda_min\n",
    "theoretical_rich_factor = (eigratio - 1) / (eigratio + 1)\n",
    "theoretical_cheb_factor = (eigratio**0.5 - 1) / (eigratio**0.5 + 1)\n",
    "ax.semilogy([0,20],[1,theoretical_cheb_factor**20],'k:',label=\"Chebyshev convergence factor\")\n",
    "ax.semilogy([0,20],[1,theoretical_rich_factor**20],'k--',label=\"Richardson convergence factor\")\n",
    "_ = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 1 (4 points):** So far we have been passing in the exactly correct values of $\\lambda_{\\min}$ and $\\lambda_{\\max}$.  But in practice, we will often have computational estimates of these values.\n",
    "\n",
    "- Keep the same $A$, $r$, and $x^{(0)}$ as above, but run `chebyshev_iteration` with intentionally incorrect values of $\\lambda_{\\min}$ or $\\lambda_{\\max}$, for example $(1 \\pm \\epsilon) \\lambda_{\\min}$ or $(1 \\pm \\epsilon) \\lambda_{\\max}$.\n",
    "- Generate some residual plots for these intentionally incorrect values.\n",
    "- After you do that, answer the following three questions:\n",
    "\n",
    "1. Which is it more important to estimate well: $\\lambda_{\\min}$ or $\\lambda_{\\max}$?\n",
    "2. Is it worse to overestimate or underestimate $\\lambda_{\\min}$?\n",
    "3. Is it worse to overestimate or underestimate $\\lambda_{\\max}$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-2968897257a79616",
     "locked": false,
     "points": 1,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "k = 20\n",
    "_, rsminunder = chebyshev_iteration(A, 0.75 * lambda_min, lambda_max, b, x0, k)\n",
    "minundernorms = [np.linalg.norm(r) for r in rsminunder]\n",
    "_, rsminover = chebyshev_iteration(A, 1.33 * lambda_min, lambda_max, b, x0, k)\n",
    "minovernorms = [np.linalg.norm(r) for r in rsminover]\n",
    "_, rsmaxunder = chebyshev_iteration(A, lambda_min, 0.75 * lambda_max, b, x0, k)\n",
    "maxundernorms = [np.linalg.norm(r) for r in rsmaxunder]\n",
    "_, rsmaxover = chebyshev_iteration(A, lambda_min, 1.33 * lambda_max, b, x0, k)\n",
    "maxovernorms = [np.linalg.norm(r) for r in rsmaxover]\n",
    "\n",
    "ax = plt.figure(figsize=(8,5)).gca()\n",
    "ax.set(title=\"convergence histories\")\n",
    "ax.set(xlabel=\"iterate k\")\n",
    "ax.set(ylabel=\"residual norm\")\n",
    "iterates = list(range(k+1))\n",
    "ax.semilogy(iterates, minundernorms, label=\"min under\")\n",
    "ax.semilogy(iterates, minovernorms, label=\"min over\")\n",
    "ax.semilogy(iterates, maxundernorms, label=\"max under\")\n",
    "ax.semilogy(iterates, maxovernorms, label=\"max over\")\n",
    "_ = ax.legend()\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-fbd6ff78439a4368",
     "locked": false,
     "points": 3,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "1. It is more important to estimate $\\lambda_{\\max}$ well, because underesimating it can lead to divergence.\n",
    "2. It is worse to underestimate $\\lambda_{\\max}$.\n",
    "3. It is worse to overestimate $\\lambda_{\\min}$, but the convergence is not as sensitive to errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Krylov subspaces\n",
    "\n",
    "In our preparation we saw two types of projections that approximately solve $Ax = b$ that are guaranteed to be solvable:\n",
    "\n",
    "1. Orthogonal projection when $A$ is SPD:\n",
    "\n",
    "$$ x_{\\text{approx}} = V(V^T A V)^{-1} V^T b$$\n",
    "\n",
    "2. Oblique projection with $V$ and $W = AV$, which is like orthogonal projection on the normal equations:\n",
    "\n",
    "$$ x_{\\text{approx}} = V(W^T A V)^{-1} W^T b = V(V^T A^T A V)^{-1} V^T A^T b$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that for these equations it is not necessary in exact arithmetic for $V$ to be orthogonal.\n",
    "\n",
    "When $V_m$ spans $(v, Av, A^2 v, \\dots, A^{k-1} v)$, then $V_m$ is a basis for the Krylov subspace $\\mathcal{K}_m(A,v)$.\n",
    "\n",
    "Because the subspaces are nested, $\\mathcal{K}_m \\subseteq \\mathcal{K}_{m+1}$ we can create a basis $V_{m+1}$ from a basis $V_{m}$ with just one additional matrix-vector product, much like one additional iteration of the iterative methods we have already seen.\n",
    "\n",
    "We're going to do something we should never do after today: use $V_m = [v, Av, A^2v, \\dots A^{k-1} v]$ directly.  We will take $v = b - A x^{(0)}$, as is customary in Krylov subspace methods.\n",
    "\n",
    "We will create $V_{m+1} = [V_m, A v_{m-1}]$ where $v_{m-1}$ is the last vector of $V_m$.\n",
    "\n",
    "We will go from one approximate solution to the next by incrementally computing the Cholesky decomposition of $V_m^T A V_m$ or $V_m^T A^T A V_m$.\n",
    "\n",
    "This procedures is implemented as `incremental_krylov_projection`, which also returns all of the $V_m^T A V_m$ projected systems that are solved along the way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-6bf9ec4b78bb033c",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def incremental_krylov_projection(A, b, x0, k):\n",
    "    \"\"\"Incrementally solve projection problem with Krylov subspaces\n",
    "    without using an orthonormal basis of the Krylov subspace.  Return the solution history,\n",
    "    as well as project subsystems (V.T @ A @ V) that are solved at each iteration for diagnostic\n",
    "    purposes\"\"\"\n",
    "    r = b - A @ x0\n",
    "    x = x0.copy()\n",
    "    xs = [x.copy()]\n",
    "    V = np.ndarray(x.shape + (1,))\n",
    "    V[:,0] = r / np.linalg.norm(r)\n",
    "    VTRlast = V.T @ r\n",
    "    Av = A @ V[:,-1]\n",
    "    VTAV = V.T @ Av\n",
    "    L = VTAV**0.5 # initial Cholesky decomposition of VT A V\n",
    "    ylast = VTRlast / L\n",
    "    L = L.reshape((1,1))\n",
    "    y = ylast.reshape((1,1))\n",
    "    VLinvT = V.copy() / L\n",
    "    subsystems = [V.T @ A @ V]\n",
    "    for i in range(k):\n",
    "        x += ylast * VLinvT[:,-1]\n",
    "        xs.append(x.copy())\n",
    "        if i is k - 1:\n",
    "            break\n",
    "        #Av /= np.linalg.norm(Av)\n",
    "        V = np.hstack([V, Av[:,np.newaxis]])\n",
    "        subsystems.append(V.T @ A @ V)\n",
    "        VLinvT = np.hstack([VLinvT, V[:,[-1]]])\n",
    "        VTRlast = V[:,-1].T @ r\n",
    "        Av = A @ V[:,-1]\n",
    "        VTAVlst = V.T @ Av # last row of VT A V\n",
    "        Llast = solve_triangular(L,VTAVlst[:-1],lower=True)\n",
    "        l = VTAVlst[-1] - Llast.dot(Llast)\n",
    "        VLinvT[:,-1] -= VLinvT[:,:-1] @ Llast\n",
    "        VTRlast -= Llast.dot(y)\n",
    "        if l <= 0.:\n",
    "            break\n",
    "        l = l**0.5\n",
    "        ylast = VTRlast / l\n",
    "        VLinvT[:,-1] /= l\n",
    "        # Grow the Cholesky factorization\n",
    "        L = np.vstack([L,Llast[np.newaxis,:]])\n",
    "        newvec = np.zeros(L.shape[0])\n",
    "        newvec[-1] = l\n",
    "        L = np.hstack([L, newvec[:,np.newaxis]])\n",
    "        y = np.vstack([y,ylast])\n",
    "    return xs, subsystems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is code to create a random SPD matrix with a given range of eigenvalues:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-da9ed8d775c71c0c",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def random_spd(n, lambda_min, lambda_max):\n",
    "    lambdas = diags(np.linspace(lambda_min, lambda_max, n))\n",
    "    X = np.random.randn(n,n)\n",
    "    Q,_ = np.linalg.qr(X)\n",
    "    return Q.T @ lambdas @ Q"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we test the iterative orthogonal projection on a matrix with conditioner number $\\kappa(A) = 100$.  You can run this a few times and see that it usually doesn't go farther than 12 iterations even though we asked for 40.  This is because the incremental Cholesky factorization breaks down with a negative pivot that can't be factored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-c58e547593fb57d5",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "n = 40\n",
    "k = 40\n",
    "A = random_spd(n, 0.01, 1.0)\n",
    "b = np.random.rand(n)*2 - 1\n",
    "x0 = np.random.rand(n)*2 - 1\n",
    "xs, subsystems = incremental_krylov_projection(A, b, x0, k)\n",
    "rs = [np.linalg.norm(b - A @ x) for x in xs]\n",
    "ax = plt.figure(figsize=(8,5)).gca()\n",
    "ax.set(title=\"Convergence history, incremental Krylov projection\")\n",
    "ax.set(xlabel=\"iterate k\")\n",
    "ax.set(ylabel=r'$\\|r^{(k)}\\|$')\n",
    "_ = plt.semilogy(list(range(len(rs))), rs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 2 (2 points):** Try `incremental_krylov_projection` for well conditioned systems ($\\kappa(A) = \\lambda_{\\max} / \\lambda_{\\min}$ close to 1) and ill-conditioned systems ($\\kappa(A)$ large).  Answer these questions:\n",
    "\n",
    "1. Does this method solve any of the problems very accurately (close to machine precision)?\n",
    "2. If so, which ones?\n",
    "3. What explains this behavior? (Hint: look at the the projected system matrices that are solved along the way)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-476ea64314f44b85",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "Even for a well-conditioned system $A$ the naive Krylov subspace $V_m$ becomes ill-conditioned quickly, so the projected system becomes ill-conditioned very quickly.  None of them are solved to more than $\\sqrt{\\epsilon}$ accuracy."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
