{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activity 3.1: Exploring preconditioning of GMRES\n",
    "\n",
    "4 points\n",
    "\n",
    "Key takeaways:\n",
    "\n",
    "- The difference between left and right preconditioning\n",
    "- Why _non-stationary_ preconditioners have problems in non-flexible Krylov methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Collaboration statement:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install numpy scipy matplotlib\n",
    "\n",
    "from scipy.sparse.linalg import gmres, minres, LinearOperator, spsolve\n",
    "from scipy.sparse import csr_matrix, rand, eye, diags\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a random matrix `A` and right hand side `b`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-67cdc64226c975ca",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "n = 1000\n",
    "density = 0.01\n",
    "A = rand(n, n, density) + 2.75*eye(n)\n",
    "A = A - 0.9 * 0.5 * (A - A.T)\n",
    "b = np.random.rand(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`scipy.sparse.linalg.gmres` is an implementation of GMRES.  We can use it to solve systems in `A`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xhat, info = gmres(A, b, tol=1.e-2)\n",
    "print(info)\n",
    "print(\"residual norm\", np.linalg.norm(b - A @ xhat))\n",
    "print(\"right hand side norm\", np.linalg.norm(b))\n",
    "print(\"relative reduction in residual norm\", np.linalg.norm(b - A @ xhat) / np.linalg.norm(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 1 (2 points):** First implement left- and right-preconditioned gmres for yourself by calling `gmres` with the appropriate right hand side, operator, and returning the appropriate solution.\n",
    "\n",
    "**Note:** gmres accepts a preconditioner argument `M`, but you **should not use it**.  We are pretending that argument is not available.\n",
    "\n",
    "_Hint:_ you may want to use the following code, which wraps up the product of two linear operators as a composite operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-a38d44af0f25a77a",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def product_op(A, B, dtype=np.float64):\n",
    "    return LinearOperator((A.shape[0], B.shape[1]), matvec=lambda x: A @ (B @ x), dtype=dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-22e640ef693b9592",
     "locked": false,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def gmres_left_preconditioned(A, b, x0, M, **kwargs):\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        A (linear operator): matrix\n",
    "        b (ndarray): rhs\n",
    "        x0 (ndarray): initial guess\n",
    "        M (linear operator): preconditioner\n",
    "        kwargs: should be passed to `gmres`\n",
    "    Returns:\n",
    "        (ndarray, info): the solution, plus the info object returned by `gmres`\n",
    "    \"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    return gmres(product_op(M, A), M @ b, x0, **kwargs)\n",
    "    ### END SOLUTION\n",
    "\n",
    "def gmres_right_preconditioned(A, b, x0, M, **kwargs):\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        A (linear operator): matrix\n",
    "        b (ndarray): rhs\n",
    "        x0 (ndarray): initial guess\n",
    "        M (linear operator): preconditioner\n",
    "        kwargs: should be passed to `gmres`\n",
    "    Returns:\n",
    "        (ndarray, info): the solution, plus the info object returned by `gmres`\n",
    "    \"\"\"\n",
    "    ### BEGIN SOLUTION\n",
    "    r0 = b - A @ x0\n",
    "    u, info = gmres(product_op(A, M), r0, np.zeros(x0.shape), **kwargs)\n",
    "    return (x0 + M @ u, info)\n",
    "    ### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-52328ff39c2ce580",
     "locked": true,
     "points": 2,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "# If we use an exact preconditioner they should both return the true solution after one iteration\n",
    "xtrue = spsolve(A, b)\n",
    "Ainv = np.linalg.inv(A.toarray())\n",
    "x0 = np.random.randn(n)\n",
    "xleft, infoleft = gmres_left_preconditioned(A, b, x0, Ainv, restart=1, maxiter=1)\n",
    "assert(infoleft == 0)\n",
    "assert(np.linalg.norm(xleft - xtrue) < 1.e-8)\n",
    "xright, inforight = gmres_right_preconditioned(A, b, x0, Ainv, restart=1, maxiter=1)\n",
    "assert(inforight == 0)\n",
    "assert(np.linalg.norm(xright - xtrue) < 1.e-8)\n",
    "\n",
    "### BEGIN HIDDEN TESTS\n",
    "def _gmres_left_preconditioned(A, b, x0, M, **kwargs):\n",
    "    return gmres(product_op(M, A), M @ b, x0, **kwargs)\n",
    "\n",
    "def _gmres_right_preconditioned(A, b, x0, M, **kwargs):\n",
    "    r0 = b - A @ x0\n",
    "    u, info = gmres(product_op(A, M), r0, np.zeros(x0.shape), **kwargs)\n",
    "    return (x0 + M @ u, info)\n",
    "\n",
    "M = diags(np.random.rand(n))\n",
    "xleft, _ = gmres_left_preconditioned(A, b, x0, M, restart=2, maxiter=2)\n",
    "xright, _ = gmres_right_preconditioned(A, b, x0, M, restart=2, maxiter=2)\n",
    "_xleft, _ = _gmres_left_preconditioned(A, b, x0, M, restart=2, maxiter=2)\n",
    "_xright, _ = _gmres_right_preconditioned(A, b, x0, M, restart=2, maxiter=2)\n",
    "assert(np.linalg.norm(xleft - _xleft) < 1.e-8)\n",
    "assert(np.linalg.norm(xright - _xright) < 1.e-8)\n",
    "### END HIDDEN TESTS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 2 (1 point):** `scipy.sparse.linalg.gmres` does take a preconditioning argument `M`, but the documentation doesn't say whether it is left- or right-preconditioned, and the documentation is unclear about whether the stopping tolerance depends on the $\\ell_2$ or preconditioned norm.  Compare `gmres(M=...)` to your `gmres_left_preconditioned` and `gmres_right_preconditioned` and say which side `gmres` preconditions.\n",
    "\n",
    "_Hint 1:_ the two are equivalent with exact precontioning $M = A^{-1}$ and with no preconditioning $M = I$, you might want to use a random diagonal preconditioner to create a situation where they are different.\n",
    "\n",
    "_Hint 2:_ Make sure you use a fixed `maxiter` and `restart` in your comparison."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-ff2e72399644d7cc",
     "locked": false,
     "points": 1,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "`gmres` is left-preconditioned."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Activity 3 (1 point):** An iterative method `KSP(A,r)` applied to a right hand side can be thought of as an approximation of $A^{-1}$, but is a _non-stationary_ approximation of $A^{-1}$.  It won't satisfy linearity exactly, so `KSP(A, alpha * r + beta * q)` $\\neq$ `alpha * KSP(A, r) + beta * KSP(A, q)`.  Because of this,\n",
    "there is no guarantee that when we reconstruct $x$ from a Krylov subspace by $x = V y$ that `A @ KSP(A, x) == A @ KSP(A, V @ y)`, which can cause problems for `gmres`.  (This is why FGMRES was created)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's say we want to precondition $A$ with an approximate inverse of its symmetric component, which does make the system well-conditioned in this case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-59bcd1c4a60e15df",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "Asym = 0.5 * (A + A.T)\n",
    "\n",
    "print(np.linalg.cond(A.toarray()))\n",
    "print(np.linalg.cond(np.linalg.solve(Asym.toarray(), A.toarray())))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code creates an approximation to $A_{sym}^{-1}$ using minres to a fixed tolerance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-fb8f7262d8946057",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "def minres_op(Asym, tol):\n",
    "    \"\"\"Create a LinearOperator that aplies MINRES to approximately solve Asym x = r to a given relative tolerance\"\"\"\n",
    "    return LinearOperator(Asym.shape, matvec=lambda r: minres(Asym, r, x0=np.zeros(Asym.shape[0]), tol=tol)[0], dtype=Asym.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is code to let us collect residuals from `gmres`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-8569fb223c9e83e0",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "class MonitorResiduals(object):\n",
    "    def __init__(self, array):\n",
    "        self.array = array\n",
    "        \n",
    "    def __call__(self, pr_norm):\n",
    "        self.array.append(pr_norm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Modify the plot below** by adding convergence histories for different tolerances on the inner MINRES solve to see how exact the tolerance has to be before the minres preconditioning of gmres has stable convergence. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-b05c2478030ca119",
     "locked": false,
     "points": 1,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "outputs": [],
   "source": [
    "tol = 1.e-12\n",
    "M = minres_op(Asym, tol)\n",
    "unprec_residuals = []\n",
    "prec_residuals = {}\n",
    "prec_residuals[tol] = []\n",
    "_ = gmres_right_preconditioned(A, b, np.zeros(b.shape), M, tol=0., restart=5, maxiter=6,\n",
    "                               callback=MonitorResiduals(prec_residuals[tol]), callback_type='pr_norm')\n",
    "_ = gmres(A, b, np.zeros(b.shape), tol=0., restart=5, maxiter=6,\n",
    "          callback=MonitorResiduals(unprec_residuals), callback_type='pr_norm')\n",
    "\n",
    "ax = plt.figure(figsize=(13,8)).gca()\n",
    "ax.set(xlabel='iteration k')\n",
    "ax.set(ylabel=r'$\\|r_k\\|$')\n",
    "ax.set(title='Comparison of MINRES as preconditioner')\n",
    "ax.semilogy(unprec_residuals, label='unpreconditioned')\n",
    "ax.semilogy(prec_residuals[tol], label='tol={}'.format(tol))\n",
    "### BEGIN SOLUTION\n",
    "tols = [10**-i for i in range(1, 12, 2)]\n",
    "for tol in tols:\n",
    "    M = minres_op(Asym, tol)\n",
    "    prec_residuals[tol] = []\n",
    "    _ = gmres_right_preconditioned(A, b, np.zeros(b.shape), M, tol=0., restart=5, maxiter=6,\n",
    "                                   callback=MonitorResiduals(prec_residuals[tol]), callback_type='pr_norm')\n",
    "    ax.semilogy(prec_residuals[tol], label='tol={}'.format(tol))\n",
    "### END SOLUTION\n",
    "ax.legend()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
