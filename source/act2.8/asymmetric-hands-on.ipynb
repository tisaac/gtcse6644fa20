{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activity 2.8: Hands on experience with asymmetric solvers\n",
    "\n",
    "6 points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**collaboration statement:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All of the solvers for asymmetric systems $Ax = b$ that we have discussed have tradeoffs:\n",
    "\n",
    "| solver   | pro     | con |\n",
    "| ------   | ---     | ---- |\n",
    "| GMRES | optimal convergence in $\\ell_2$ | large storage, Arnoldi process $O(m)$ updates |\n",
    "| GMRES(k) | fixed storage, optimal convergence in $\\ell_2$ over $k$ iterations | stagnation |\n",
    "| CGNE | short recurrence, optimal in $A^T A$ error norm | requires $A^T$, two matvecs per iteration, squared condition number, slower convergence |\n",
    "| BiCG | short recurrence | requires $A^T$, two matvecs per iteration, no optimality condition, stagnation or breakdown |\n",
    "| QMR | short recurrence, quasi-optimal in $\\ell_2$ | requires $A^T$, two matvecs per iteration, stagnation or breakdown |\n",
    "| BiCGStab | short recurrence, does not need $A^T$ | greater rounding errors than BCG, stagnation or breakdown |\n",
    "| TFQMR | short recurrence, quasi-optimal in $\\ell_2$, does not need $A^T$ | greater rounding errors than QMR, stagnation or breakdown |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this activity you will work on the PACE-ICE cluster.  You will compare these methods on one of four matrices.\n",
    "\n",
    "We will make our comparisons on matrices that are relatively small and over-decomposed: we will use the most MPI parallelism that PACE-ICE lets us use, which is $P=128$.  This will minimize the time spent in the matvec, but will emphasize the time spent in communication."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Working in teams (one team per matrix), start a parallel job one the PACE-ICE cluster with the following command:\n",
    "\n",
    "```\n",
    "qsub -d /storage/home/hcocice1/tisaac3/share/cse6644/examples/ex2.8team1 -q coc-ice-multi -n -l nodes=4:ppn=32,walltime=00:30:00 -I\n",
    "qsub -d /storage/home/hcocice1/tisaac3/share/cse6644/examples/ex2.8team2 -q coc-ice-multi -n -l nodes=4:ppn=32,walltime=00:30:00 -I\n",
    "qsub -d /storage/home/hcocice1/tisaac3/share/cse6644/examples/ex2.8team3 -q coc-ice-multi -n -l nodes=4:ppn=32,walltime=00:30:00 -I\n",
    "qsub -d /storage/home/hcocice1/tisaac3/share/cse6644/examples/ex2.8team4 -q coc-ice-multi -n -l nodes=4:ppn=32,walltime=00:30:00 -I\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 1. (2 points)** Try out the above solvers on the matrix you have been assigned using PETSc.\n",
    "First try to understand the convergence behavior by solving a random system in a verbose way that outputs lots of diagnostic information, like this:\n",
    "\n",
    "```\n",
    "make run_matrix_1_verbose ALL_OPTIONS=\"-ksp_type XXX\"\n",
    "```\n",
    "\n",
    "This will output the **true** and **computed** residual history of the method:\n",
    "\n",
    "```\n",
    "  0 KSP preconditioned resid norm 5.510323644525e+00 true resid norm 5.510323644525e+00 ||r(i)||/||b|| 1.000000000000e+00\n",
    "  1 KSP preconditioned resid norm 5.288795703021e+00 true resid norm 5.288795703021e+00 ||r(i)||/||b|| 9.597976533148e-01\n",
    "  2 KSP preconditioned resid norm 5.277756905030e+00 true resid norm 5.277756905030e+00 ||r(i)||/||b|| 9.577943593701e-01\n",
    "  3 KSP preconditioned resid norm 5.245477192714e+00 true resid norm 5.245477192714e+00 ||r(i)||/||b|| 9.519363164677e-01\n",
    "```\n",
    "\n",
    "- The **true** history will actually compute $\\|r_i\\| = \\|b - A x_i$ at iteration $i$ and return the $\\ell_2$ norm\n",
    "- The **computed** history will have the estimate of $\\|r_i\\|$ that the matrix uses to determine convergence.  These can diverge due to rounding errors, especially because of loss of orthogonality in the Arnoldi/Lanczos/Bi-Lanczos process\n",
    "\n",
    "It will also print out whether the method actually converged or not and a short summary of the matrix and the method used (you can use this to check if your options were entered correctly):\n",
    "\n",
    "```\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 258\n",
    "KSP Object: 128 MPI processes\n",
    "  type: gmres\n",
    "    restart=75, using Classical (unmodified) Gram-Schmidt Orthogonalization with no iterative refinement\n",
    "    happy breakdown tolerance 1e-30\n",
    "  maximum iterations=10000, initial guess is zero\n",
    "  tolerances:  relative=1e-05, absolute=1e-50, divergence=10000.\n",
    "  left preconditioning\n",
    "  using PRECONDITIONED norm type for convergence test\n",
    "PC Object: 128 MPI processes\n",
    "  type: none\n",
    "  linear system matrix = precond matrix:\n",
    "  Mat Object: 128 MPI processes\n",
    "    type: mpiaij\n",
    "    rows=90, cols=90\n",
    "    total: nonzeros=1746, allocated nonzeros=1746\n",
    "    total number of mallocs used during MatSetValues calls=0\n",
    "      not using I-node (on process 0) routines\n",
    "```\n",
    "\n",
    "### 1. GMRES(k)\n",
    "\n",
    "PETSc does not have unlimited GMRES, but if you choose GMRES(k) with k bigger than the system size it ought to be equivalent.\n",
    "\n",
    "You should invoke GMRES(k) with the following options:\n",
    "\n",
    "```\n",
    "ALL_OPTIONS=\"-ksp_type {gmres,pgmres} -ksp_gmres_restart K -ksp_gmres_cgs_refinement_type {refine_never,refine_ifneeded,refine_always}\"\n",
    "```\n",
    "\n",
    "- Choose classic GMRES with `-ksp_type gmres`, or the pipelined version with `-ksp_type pgmres`\n",
    "- Choose the restart size with `-ksp_gmres_restart k`\n",
    "- PETSc uses the Classical Gram-Schmidt process to compute the Arnoldi vectors because it is more paralle, but you can invoke the iterative refined orthogonalization discussed in the notes 2.3 _never_ (`-ksp_gmres_cgs_refinement_type refine_never`), _if needed_ (`-ksp_gmres_cgs_refinement_type refine_ifneeded`), or _always_ (`-ksp_gmres_cgs_refinement_type refine_always`).  You will know that refinement could be useful if the computed residual diverges significantly from the true residual.\n",
    "\n",
    "**NOTE:** The pipelined version `pgmres` does not refine the classical Gram-Schmidt process: if orthogonality is lost, it will not be regained until the next restart.\n",
    "\n",
    "### 2. Conjugate Gradient for the normal equations\n",
    "\n",
    "```\n",
    "ALL_OPTIONS=\"-ksp_type cgne\"\n",
    "```\n",
    "\n",
    "### 3. Biconjugate Gradient (BCG / BiCG)\n",
    "\n",
    "```\n",
    "ALL_OPTIONS=\"-ksp_type bicg\"\n",
    "```\n",
    "\n",
    "### 4. Stabilized biconjugate gradient (BiCGStab)\n",
    "\n",
    "Original and pipelined variants\n",
    "\n",
    "```\n",
    "ALL_OPTIONS=\"-ksp_type {bcgs,pipebcgs}\"\n",
    "```\n",
    "\n",
    "### 5. Transpose-free quasi-minimal residual methods (TFQMR)\n",
    "\n",
    "```\n",
    "ALL_OPTIONS=\"-ksp_type tfqmr\"\n",
    "```\n",
    "\n",
    "Paste the options and output of your run that required **the fewest iterations** below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-81282bf3d5491920",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "```\n",
    "YOUR OUTPUT HERE\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 2 (2 points):** Now run the same solvers but timing for performance: the solve will be performed 10 times, and you should focus on the fastest of the ten.\n",
    "\n",
    "Example:\n",
    "\n",
    "```\n",
    "make run_matrix_1_measure ALL_OPTIONS=\"-ksp_type gmres -ksp_gmres_restart 30\"\n",
    "for (( i=1; i<=10; i++ )); do \\\n",
    "\tPETSC_OPTIONS=\"-ksp_converged_reason -log_view -ksp_type gmres -ksp_gmres_restart 30 -f tols90.petsc -b_in_f 0 -pc_type none\" PYTHONPATH=/storage/home/hcocice1/tisaac3/opt/pace-ice/root/usr/local/pace-apps/manual/packages/anaconda3/2020.02/lib/python3.7/site-packages:/usr/lib/oracle/11.2/lib64/python2.7/site-packages mpirun -np 128 -env MV2_ENABLE_AFFINITY 1 ./ex6 | sed -n -e \"/^ 2:    Linear solve/p\" -e \"/^Linear solve /p\"; \\\n",
    "done\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 1.5756e-01  18.9%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.0896e-02   8.0%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.4296e-02   8.2%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 7.3977e-02  11.2%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.2568e-02   7.9%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.2424e-02   7.8%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.1248e-02   8.0%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.0899e-02   8.2%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.4314e-02   8.2%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "Linear solve converged due to CONVERGED_RTOL iterations 269\n",
    " 2:    Linear solve: 5.1098e-02   7.3%  2.1957e+06  99.8%  4.637e+05  98.8%  8.000e+00       98.7%  5.470e+02  93.3%\n",
    "```\n",
    "\n",
    "THe first number after `Linear solve: ` is the runtime.\n",
    "\n",
    "Paste the options and output of your command that achieved the **smallest runtime** below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-751c16fa9ee5b12a",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "```\n",
    "YOUR OUTPUT HERE\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 3 (2 points):**  Answer these questions:\n",
    "\n",
    "1. Full GMRES will often be faster than the other options if $k$ is large enough.  If that is true for your matrix, what is the _smallest k_ where GMRES(k) is faster than the other options?\n",
    "\n",
    "2. Did iterative refinement of classical Gram-Schmidt help with the convergence of your matrix?\n",
    "\n",
    "3. Did pipelining decrease the runtime of GMRES or BiCGStab?\n",
    "\n",
    "4. Did any of the methods diverge for your matrix?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-77377745a010c9ca",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "Your answers here."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
