const char help[] = "Create the ISColoring for the matrix.\n";

#include <petscviewer.h>
#include <petscmat.h>

int
main(int argc, char **argv)
{
  Mat            A;
  PetscViewer    viewer;
  MatColoring    matcoloring;
  ISColoring     iscoloring;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  ierr = MatCreate(PETSC_COMM_WORLD, &A);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, "G3_circuit.petsc", FILE_MODE_READ, &viewer);CHKERRQ(ierr);
  ierr = MatLoad(A, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = MatColoringCreate(A, &matcoloring);CHKERRQ(ierr);
  ierr = MatColoringSetFromOptions(matcoloring);CHKERRQ(ierr);
  ierr = MatColoringApply(matcoloring, &iscoloring);CHKERRQ(ierr);
  {
    PetscInt ncolors;
    IS *color_set;

    ierr = ISColoringGetIS(iscoloring, PETSC_USE_POINTER, &ncolors, &color_set);CHKERRQ(ierr);
    for(PetscInt i = 0; i < ncolors; i++) {
      PetscViewer writer;
      char filename[BUFSIZ];

      ierr = PetscSNPrintf(filename, BUFSIZ-1, "G3_circuit_color_%D.petsc", i);CHKERRQ(ierr);
      ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &writer);CHKERRQ(ierr);
      ierr = ISView(color_set[i], writer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&writer);CHKERRQ(ierr);
    }
    ierr = ISColoringRestoreIS(iscoloring, PETSC_USE_POINTER, &color_set);CHKERRQ(ierr);
  }
  ierr = ISColoringDestroy(&iscoloring);CHKERRQ(ierr);
  ierr = MatColoringDestroy(&matcoloring);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return 0;
}
