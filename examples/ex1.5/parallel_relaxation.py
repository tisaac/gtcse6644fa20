from petsc4py import PETSc

class ParallelGaussSeidel(object):
    def __init__(self):
        self.setup_called = False

    def setup(self, pc):
        if self.setup_called:
            return
        self.setup_called = True
        A, P = pc.getOperators()
        self.pcf = PETSc.PC().create(A.comm)
        self.pcf.setType(self.pcf.Type.FIELDSPLIT)
        self.pcf.setOperators(A, P)
        self.pcf.setFieldSplitType(self.pcf.CompositeType.MULTIPLICATIVE)

    def view(self, pc, viewer):
        viewer.pushASCIITab()
        self.pcf.view(viewer)
        viewer.popASCIITab()

    def apply(self, pc, x, y):
        self.setup(pc)
        self.pcf.apply(x, y)


class LexicographicGaussSeidel(ParallelGaussSeidel):
    def __init__(self):
        super(LexicographicGaussSeidel,self).__init__()
        self.lex_setup_called = False

    def setup(self, pc):
        if self.lex_setup_called:
            return
        self.lex_setup_called = True
        super(LexicographicGaussSeidel,self).setup(pc)
        A, _ = self.pcf.getOperators()
        num_procs = A.comm.size
        rank = A.comm.rank
        isets = []
        for i in range(num_procs):
            if i == rank:
                indices = list(range(*(A.getOwnershipRange())))
            else:
                indices = []
            iset = PETSc.IS().createGeneral(indices, comm=A.comm)
            isets.append((None, iset))
        self.pcf.setOptionsPrefix("LGS_")
        self.pcf.setFieldSplitIS(*isets)
        self.pcf.setFromOptions()


class MulticolorGaussSeidel(ParallelGaussSeidel):
    def __init__(self):
        super(MulticolorGaussSeidel,self).__init__()
        self.mc_setup_called = False

    def setup(self, pc):
        import bisect

        if self.mc_setup_called:
            return
        self.mc_setup_called = True
        super(MulticolorGaussSeidel,self).setup(pc)
        A, _ = self.pcf.getOperators()
        comm = A.comm
        rank = comm.rank
        first, last = A.getOwnershipRange()
        isets = []
        for color in range(8):
            viewer = PETSc.Viewer().createBinary('G3_circuit_color_{}.petsc'.format(color), 'r')
            isetdistributed = PETSc.IS().load(viewer)
            isetredundant = isetdistributed.allGather()
            iindices = isetredundant.getIndices()
            ifirst = bisect.bisect_left(iindices,first)
            ilast = bisect.bisect_left(iindices,last)
            isglobal = PETSc.IS().createGeneral(iindices[ifirst:ilast], comm=A.comm)
            isets.append((None, isglobal))
        self.pcf.setOptionsPrefix("MGS_")
        self.pcf.setFieldSplitIS(*isets)
        self.pcf.setFromOptions()


if __name__ == '__main__':
    viewer = PETSc.Viewer().createBinary('G3_circuit.petsc', 'r')
    A = PETSc.Mat().load(viewer)
    ksp = PETSc.KSP().create(PETSc.COMM_WORLD)
    ksp.setType(ksp.Type.RICHARDSON)
    pc = ksp.getPC()
    pc.setType(pc.Type.JACOBI)
    pc.setOperators(A)
    ksp.setTolerances(rtol=1.e-5, max_it=10000)
    ksp.setNormType(ksp.NormType.UNPRECONDITIONED)
    ksp.setMonitor(lambda _ksp, _its, _norm: None)
    ksp.setFromOptions()
    rhs = A.createVecRight()
    sol = A.createVecRight()
    sol.set(1.)
    A.mult(sol, rhs)
    sol.set(0.)
    with PETSc.Log.Stage("Solve"):
        ksp.solve(rhs, sol)
    ksp.view()
    #PETSc._finalize()
